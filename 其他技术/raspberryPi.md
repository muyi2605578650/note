[toc]

4代B是树莓派的最新版本，Cortex-A72 1.5GHz 64bit 4核 CPU，RAM最大8GB，蓝牙5.0，4个USB（其中两个USB3.0），2个micro-HDMI端口（最高双屏4K显示）

![image-20220112122643493](./resource/image-20220112122643493.png)

![image-20220112123153059](./resource/image-20220112123153059.png)

总结了以下几点树莓派非常流行的应用：

- **【学习办公】**刷入Ubuntu、Centos 等系统，外接键鼠、显示器就可以当做 Linux 电脑日常使用，编辑文档、浏览网页、玩游戏都不在话下。
- **【生活娱乐】**作为[影音播放器](https://www.smzdm.com/fenlei/gaoqingbofangqi/)、[电视盒子](https://www.smzdm.com/fenlei/gaoqingbofangqi/)，建立家庭媒体中心。
- **【服务器】**用来做 NAS 文件服务器、WEB服务器、软路由，搭建个人网站，代理服务器等等。
- **【智能家居】**树莓派+Home Assistant可以强大的智能家居系统 ，将市面上几乎所有的智能家居设备都统一管理。
- **【工控设备】**树莓派的 GPIO 引脚赋予极客无穷的发挥空间，改造传统设备、DIY无人机、机器人、游戏机，创意无限。



## 配置网络

首先将内存卡从树莓派中拔出来插入到读卡器中，读卡器插入电脑

在boot分区中新建一个ssh文件（**注意没有后缀！！！**）

这就开通了ssh了

接下来再新建一个wpa_supplicant.conf文件

修改并写入以下内容...

```conf
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=CN
 
network={
        ssid="你的无线网名字"
        psk="密码"
        key_mgmt=WPA-PSK
}
 
network={
        ssid="你的其他无线网名字"
        psk="密码"
        key_mgmt=WPA-PSK
}
```

- https://www.zhihu.com/question/449295622
- https://blog.csdn.net/u011198687/article/details/123312666

# 其他命令

- date：查看时间
- sudo dpkg-reconfigure tzdata：设置时区
