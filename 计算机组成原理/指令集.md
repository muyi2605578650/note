## **指令集分类**

从大类来分，一般将指令集分为精简指令集和复杂指令集。

- 复杂指令集，即CISC指令集Complex Instruction Set Computer：（http://baike.baidu.com/view/1177592.htm）

- - 典型的 CICS 指令集的 CPU 有：Intel 的 x86 指令集，以及现在的 AMD 的 x86-64 指令集

- 精简指令集，即RISC指令集reduced instruction set computer：（http://baike.baidu.com/view/981569.htm）

- - 这种指令集的特点是指令数目少，每条指令都采用标准字长、执行时间短、中央处理器的实现细节对于机器级程序是可见的。
  - 典型的 RICS 指令集的 CPU 有：ARM、MIPS 等

- 最开始，Intel X86 的第一个 CPU 定义了第一套指令集，这就是最开始的指令集，后来，一些公司发现很多指令并不常用，所以决定设计一套简洁高效的指令集，称之为 RICS 指令集，从而将原来的 Intel X86 指令集定义为 CISC 指令集

- 通俗的理解，RICS 指令集是针对 CISC 指令集中的一些常用指令进行优化设计，放弃了一些复杂的指令，对于复杂的功能，需要通过组合指令来完成。自然，两者的使用场合不一样，对于复杂的系统，CISC 更合适，否则，RICS 更合适，且低功耗。