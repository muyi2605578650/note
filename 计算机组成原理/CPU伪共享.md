## 伪共享的概念

- CPU缓存一般是多级架构
- 缓存是由`缓存行`组成的，通常是 64 字节（常用处理器的缓存行是 64 字节的，比较旧的处理器缓存行是 32 字节），并且它有效地引用主内存中的一块地址
- Java 的 long 类型是 8 字节，因此在一个缓存行中可以存 8 个 long 类型的变量。在程序运行的过程中，缓存每次更新都从主内存中加载连续的 64 个字节。因此，如果访问一个 long 类型的数组时，当数组中的一个值被加载到缓存中时，另外 7 个元素也会被加载到缓存中
- 假设如果有个 long 类型的变量 a，并且还有一个 long 类型的变量 b 紧挨着它，那么当加载 a 的时候将同时加载 b
- 一个线程更新完 a 后（刷新到主内存）其它所有`包含 a 的缓存行`都将失效，因为其它缓存中的 a 不是最新值了，`当然也包括b，因为它们在同一个缓存行`，另一个线程对b操作时，发现这个缓存行失效了，就需要从主内存中重新加载
- 这样就出现了一个问题，b 和 a 完全不相干，每次却要因为 a 的更新从主内存重新读取，明明他们共享一个缓存行，却没有达到共享的目的，这就是`伪共享`

## Java演示伪共享

- 以下程序证明了伪共享的存在，没加`@Contended`的程序运行时间更长

```java
public class FalseSharingTest {

    public static void main(String[] args) throws InterruptedException {
        testPointer(new Pointer());
    }

    private static void testPointer(Pointer pointer) throws InterruptedException {
        long start = System.currentTimeMillis();
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 100000000; i++) {
                pointer.x = pointer.x + 1;
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 100000000; i++) {
                pointer.y++;
            }
        });

        t1.start();
        t2.start();
        t1.join();
        t2.join();

        System.out.println(System.currentTimeMillis() - start);
        System.out.println(pointer);
    }
}

class Pointer {
    @Contended
    volatile long x;
//    long p1, p2, p3, p4, p5, p6, p7;
    @Contended
    volatile long y;
}
```

## 如何解决伪共享问题

- 在两个 long 类型的变量之间再加 7 个 long 类型
- 使用 `@sun.misc.Contended` 注解，同时在JVM启动参数加上`-XX:-RestrictContended`才会生效

## Java中有哪些地方涉及到了伪共享

- ConcurrentHashMap，里面的 size() 方法使用的是分段的思想来构造的，每个段使用的类是 CounterCell，它的类上就有 @Contended 注解
- LongAdder 也使用了@Contended注解避免伪共享

## 参考

- [参考文章](https://zhuanlan.zhihu.com/p/65394173)