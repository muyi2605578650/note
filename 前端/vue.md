# 前端4大特性

- 逻辑
- 事件：Jquery、vue、react
- 视图：html、css、bootstrap
- 通信：ajax、xhr（原生js实现）、axios（vue的）

# 前端杂烩

## 什么是 CSS 预处理器

- CSS 预处理器定义了一种新的语言，其基本思想是，用一种专门的编程语言，为 CSS 增加了一些编程的特性，将 CSS 作为目标生成文件，然后开发者就只需要使用这种语言进行 CSS 的编码工作。转化成通俗易懂的话来说就是 “**用一种专门的编程语言，进行 Web 页面样式设计，再通过编译器转化为正常的 CSS 文件，以供项目使用”**。
- 常用的 CSS 预处理器有哪些
  - SASS：基于 Ruby ，通过服务端处理，功能强大。解析效率高。需要学习 Ruby 语言，上手难度高于 LESS。
  - LESS：基于 NodeJS，通过客户端处理，使用简单。功能比 SASS 简单，解析效率也低于 SASS，但在实际开发中足够了，所以如果我们后台人员如果需要的话，建议使用 LESS。

## **Native 原生 JS 开发**（待记忆）

- 原生 JS 开发，也就是让我们按照【ECMAScript】标准的开发方式，简称 ES，特点是所有浏览器都支持。截至到当前，ES 标准以发布如下版本
  - ES3
  - ES4（内部，未正式发布）
  - ES5（全浏览器支持）
  - ES6（常用，当前主流版本：webpack 打包成为 ES5 支持）
  - ES7
  - ES8
  - ES9（草案阶段）

## **TypeScript ——微软的标准**

- TypeScript 是一种由微软开发的自由和开源的编程语言。它是 JavaScript 的一个超集， 而且本质上向这个语言添加了可选的静态类型和基于类的面向对象编程。由安德斯 · 海尔斯伯格 (C#、Delphi、TypeScript 之父； .NET 创立者) 主导。该语言的特点就是除了具备 ES 的特性之外还纳入了许多不在标准范围内的新特性，所以会导致很多浏览器不能直接支持 TypeScript 语法， 需要编译后 (编译成 JS) 才能被浏览器正确执行。

## **JavaScript 框架**

- JQuery：大家熟知的 JavaScript 库，优点就是简化了 DOM 操作，缺点就是 DOM 操作太频繁，影响前端性能；在前端眼里使用它仅仅是为了兼容 IE6，7，8；
- Angular：Google 收购的前端框架，由一群 Java 程序员开发，其特点是`将后台的 MVC 模式搬到了前端`并增加了`模块化开发`的理念，与微软合作，采用了 TypeScript 语法开发；对后台程序员友好，对前端程序员不太友好；最大的缺点是版本迭代不合理（如 1 代–>2 代，除了名字，基本就是两个东西；截止发表博客时已推出了 Angular6）
- React：Facebook 出品，一款高性能的 JS 前端框架；特点是`提出了新概念 【虚拟 DOM】用于减少真实 DOM 操作，在内存中模拟 DOM 操作`，有效的提升了前端渲染效率；缺点是使用复杂，因为需要额外学习一门【JSX】语言；
- Vue：一款`渐进式` JavaScript 框架，所谓渐进式就是逐步实现新特性的意思，如实现模块化开发、路由、状态管理等新特性。其特点是`综合了 Angular（模块化）和 React(虚拟 DOM) 的优点`；
- Axios：前端通信框架；因为 `Vue 的边界很明确，就是为了处理 DOM，所以并不具备通信能力`，此时就需要额外使用一个通信框架与服务器交互；当然也可以直接选择使用 jQuery 提供的 AJAX 通信功能；

## 三端同一 ——混合开发（Hybrid App）

- 主要目的是实现一套代码三端统一（PC、Android：.apk、iOS：.ipa）并能够调用到设备底层硬件（如：传感器、GPS、摄像头等），打包方式主要有以下两种：
  - 云打包：HBuild -> HBuildX，DCloud 出品；API Cloud
  - 本地打包： Cordova（前身是 PhoneGap）

## Node.JS（待记忆）

- 前端人员为了方便开发也需要掌握一定的后端技术，就出现了 Node JS 这样的技术。Node JS 的作者已经声称放弃 Node JS，开始开发全新架构的 Deno。既然是后台技术，那肯定也需要框架和项目管理工具， Node JS 框架及项目管理工具如下：
  - Express：Node JS 框架
  - Koa：Express 简化版
  - NPM：项目综合管理工具，类似于 Maven
  - YARN：NPM 的替代方案，类似于 Maven 和 Gradle 的关系

## **Flutter**

- Flutter 是谷歌的移动端 UI 框架， 可在极短的时间内构建 Android 和 iOS 上高质量的原生级应用。Flutter 可与现有代码一起工作， 它被世界各地的开发者和组织使用， 并且 Flutter 是免费和开源的。

## MVVM(异步通信为主)

- Model、View、View Model
- 为了降低前端开发复杂度，涌现了大量的前端框架，比如：`Angular JS`、`React`、`Vue.js`、`Ember JS`等， 这些框架总的原则是先按类型分层， 比如 Templates、Controllers、Models， 然后再在层内做切分，如下图

![image-20220223161949470](./resource/image-20220223161949470.png)

## Thymeleaf

- Thymeleaf 是适用于 Web 和独立环境的现代服务器端 Java 模板引擎，能够处理 HTML，XML，JavaScript，CSS 甚至纯文本。

## xhr

- 全称为XMLHttpRequest，用于与服务器交互[数据](https://so.csdn.net/so/search?q=数据&spm=1001.2101.3001.7020)，是ajax功能实现所依赖的对象，jquery中的ajax就是对 xhr的封装。

# 什么是 MVVM

- MVVM（`Model-View-ViewModel`）是一种软件设计模式，是一种简化用户界面的**事件驱动编程方式**。由 John Gossman 于2005年在他的博客上发表。
- MVVM 源自于经典的 MVC（Model-View-Controller）模式，主要目的是`分离视图（View）和模型（Model）`，核心是 ViewModel 层，其作用如下：
  - 向上与视图层进行双向数据绑定
  - 向下与 Model 层通过接口请求进行数据交互
- 由于实现了双向绑定， ViewModel 的内容会实时展现在 View 层， 这是激动人心的， 因为前端开发者`再也不必低效又麻烦地通过操纵 DOM 去更新视图`
- 当下流行的 MVVM 框架有`Vue.js`，`Angular JS`

<img src="./resource/image-20220223162814132.png" alt="image-20220223162814132"  />

![image-20220223163442403](./resource/image-20220223163442403.png)

# MVVM的好处

- **低耦合**：视图（View）可以独立于 Model 变化和修改，一个 ViewModel 可以绑定到不同的 View 上，当 View 变化的时候 Model 可以不变，当 Model 变化的时候 View 也可以不变。
- **可复用**：你可以把一些视图逻辑放在一个 ViewModel 里面，让很多 View 重用这段视图逻辑。
- **独立开发**：开发人员可以专注于业务逻辑和数据的开发（ViewMode），设计人员可以专注于页面设计。
- **可测试**：界面素来是比较难以测试的，而现在测试可以针对 ViewModel 来写。

# Vue概述

- 一款`渐进式` JavaScript 框架，所谓渐进式就是逐步实现新特性的意思，如实现模块化开发、路由、状态管理等新特性。
- Vue就是 MVVM 中的 ViewModel 层的实现者，他的核心就是实现了 DOM 监听与数据绑定
- 特性：
  - 轻量级， 体积小是一个重要指标。Vue.js 压缩后有只有 20 多 kb(Angular 压缩后 56kb+，React 压缩后 44kb+)
  - 移动优先。更适合移动端， 比如移动端的 Touch 事件
  - 易上手，学习曲线平稳，文档齐全
  - 吸取了` Angular(模块化) 和 React(虚拟 DOＭ)` 的长处， 并拥有自己独特的功能，如：计算属性
  - 开源，社区活跃度高

# Vue的几个属性

- **el属性**：用来指示vue编译器从什么地方开始解析 vue的语法，可以说是一个占位符。
- **data属性**：用来组织从view中抽象出来的属性，可以说将视图的数据抽象出来存放在data中。
- template属性：用来设置模板，会替换页面元素，包括占位符。
- methods属性：放置页面中的业务逻辑，js方法一般都放置在methods中
- render属性：创建真正的Virtual Dom
- computed属性：用来计算
- watch属性：监听data中数据的变化

  - watch:function(new,old){}
  - 两个参数，一个返回新值，一个返回旧值

# Vue基础

- 可使用IDEA来学习Vue，下载vue.js插件即可

## 初识ViewModel

```html
<div id="app">
    {{message}}
    <br>
    <span v-bind:title="message">
        鼠标悬停几秒钟查看此处动态绑定的提示信息！
    </span>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.min.js"></script>
<script>
    var vm = new Vue({
        el: '#app',
        data: {
            message: 'hello,Vue!'
        }
    });
</script>
```

## if else语句

```html
<div id="app">
    <h1 v-if="ok">Yes</h1>
    <h1 v-else>No</h1>

    <h1 v-if="type==='A'">A</h1>
    <h1 v-else-if="type==='B'">B</h1>
    <h1 v-else-if="type==='D'">D</h1>
    <h1 v-else>C</h1>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.min.js"></script>
<script type="text/javascript">
    var vm = new Vue({
        el:"#app",
        /*Model：数据*/
        data:{
            ok: true,
            type: 'A'
        }
    });
</script>
```

## for循环

```html
<div id="app">
    <li v-for="(item,index) in items">
        {{item.message}}---{{index}}
    </li>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.min.js"></script>
<script type="text/javascript">
    var vm = new Vue({
        el:"#app",
        data:{
            items:[
                {message:'狂神说ava'},
                {message:'狂神说前端'},
                {message:'狂神说运维'}
            ]
        }
    });
</script>
```

## 事件绑定

```html
<div id="app">
    <button v-on:click="sayHi">点我</button>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.min.js"></script>
<script>
    var vm = new Vue({
        el: '#app',
        data: {
            message: 'hello,Vue!'
        },
        methods: {
            sayHi:function() {
                alert(this.message)
            }
        }
    });
</script>
```

## 双向绑定

```html
<div id="app">
    输入的文本：<input v-model="message" type="text"><br>
    {{message}}
    <input type="checkbox" id="checkbox" v-model="checked">
    &nbsp;&nbsp;
    <label for="checkbox">{{checked}}</label><br>
    性别：
    <input type="radio" checked value="男" v-model="sex">男
    <input type="radio" value="女" v-model="sex">女
    <p>选中了：{{sex}}</p><br>
    下拉框:
    <select v-model="pan">
        <option value="" disabled>---请选择---</option>
        <option>A</option>
        <option>B</option>
        <option>C</option>
        <option>D</option>
    </select>
    <span>value:{{pan}}</span>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.min.js"></script>
<script>
    var vm = new Vue({
        el: '#app',
        data: {
            message: '123',
            checked: false,
            sex: '男',
            pan: ''
        }
    });
</script>
```

## Vue组件

- 组件是可复用的Vue实例， 说白了就是一组可以重复使用的模板
- 通常一个应用会以一棵嵌套的组件树的形式来组织

<img src="./resource/image-20220224164457043.png" alt="image-20220224164457043" style="zoom:50%;" />

```html
<div id="app">
    <!--组件，传递给组件中的值：props-->
    <space v-for="item in items" v-bind:prop1="item"></space>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.min.js"></script>
<script>
    // 定义一个vue组件component组件
    Vue.component("space",{
        props: ['prop1','prop2'], //这是组件的属性列表
        template:  `<li>{{prop1}}</li>`
    })
    var vm = new Vue({
        el: '#app',
        data: {
            items: ['Java','Linux','前端']
        }
    });
</script>
```

## Axios异步通信

- 少用 jQuery， 因为它操作 Dom 太频繁!

```html
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <!--v-cloak 解决闪烁问题-->
    <style>
        [v-cloak]{
            display: none;
        }
    </style>
</head>
<body>
<div id="vue" v-cloak>
    {{info}}
    <div>地名：{{info.name}}</div>
    <div>地址：{{info.address.country}}--{{info.address.city}}--{{info.address.street}}</div>
    <div>链接：<a v-bind:href="info.url" target="_blank">{{info.url}}</a> </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript">
    var vm = new Vue({
        el:"#vue",
        data: {},//这个data是属性
        data(){ //这个是函数 
            return{
                info:{ //info的数据结构必须和响应回来的数据格式匹配
                    name:null,
                    address:{
                        country:null,
                        city:null,
                        street:null
                    },
                    url:null
                }
            }
        },
        mounted(){//钩子函数,链式编程 ES6新特性
            axios.get('../data.json').then(response=>(this.info=response.data));
        }
    });
</script>
```

## Vue生命周期

- Vue 实例有一个生命周期，在整个生命周期中，它提供了一系列的事件（又叫钩子），可以让我们在事件触发时注册 JS 方法，可以让我们用自己注册的 JS 方法控制整个大局
- 这些事件响应方法中的 this 指向的是 Vue 的实例。
  - **beforeCreate（创建前）** ：组件实例被创建之初，组件的属性生效之前
  - **created（创建后）** ：组件实例已经完全创建，属性也绑定，但真实 dom 还没有生成，$el 还不可用
  - **beforeMount（挂载前）** ：在挂载开始之前被调用：相关的 render 函数首次被调用
  - **mounted（挂载后）** ：在el 被新创建的 vm.$el 替换，并挂载到实例上去之后调用该钩子
  - **beforeUpdate（更新前）** ：组件数据更新之前调用，真实DOM还没被渲染
  - **update（更新后）** ：组件数据更新之后
  - **activated（激活前）** ：keep-alive专属，组件被激活时调用
  - **deactivated（激活后）** ：keep-alive专属，组件被销毁时调用
  - **beforeDestory（销毁前）** ：组件销毁前调用
  - **destoryed（销毁后）** ：组件销毁前调用
- [参考](https://juejin.cn/post/7025150810640089119)

![20200616222020393](./resource/20200616222020393.png)

## Vue计算属性

- 简单说，它是一个能够将计算结果缓存起来的属性，可以想象为缓存
- 计算属性的主要特性就是为了将不经常变化的计算结果进行缓存，以节约我们的系统开销
- methods：定义方法， 调用方法使用 currentTime1()， 需要带括号
- computed：定义计算属性， 调用属性使用 currentTime2
- methods 和 computed 里的东西不能重名
- 如果方法中的值发生了变化，则缓存就会刷新! 可以在控制台使用vm.message=”test"， 改变下数据的值，再次测试观察效果!

```html
<div id="app">
    <p>currentTime1:{{currentTime1()}}</p>
    <p>currentTime2:{{currentTime2}}</p>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.min.js"></script>
<script type="text/javascript">
    var vm = new Vue({
        el:"#app",
        data:{
            message:"hello"
        },
        methods:{
            currentTime1:function(){
                return Date.now();//返回一个时间戳
            }
        },
        computed:{ 
            currentTime2:function(){//计算属性：methods，computed方法名不能重名，重名之后，只会调用methods的方法
                this.message;
                return Date.now();//返回一个时间戳
            }
        }
    });
</script>
```

## Vue插槽

- 使用`<slot>`元素作为承载分发内容的出口，作者称其为插槽，可以应用在组合组件的场景中
- 注：vue中的v-bind:可简写为:，v-on:可简写为@

```html
<body>
<div id="vue">
    <todo>
        <todo-title slot="todo-title" v-bind:title="title"></todo-title>
        <todo-items slot="todo-items" v-for="(item, i) in todoItems" :item="item" :index="i"></todo-items>
    </todo>
</div>
<!--1.导入Vue.js-->
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.min.js"></script>
<script type="text/javascript">
    Vue.component('todo',{
        template:'<div>\
                <slot name="todo-title"></slot>\
                <ul>\
                    <slot name="todo-items"></slot>\
                </ul>\
            </div>'
    });
    Vue.component('todo-title',{
        props:['title'],
        template:'<div>{{title}}</div>'
    });
    //这里的index，就是数组的下标，使用for循环遍历的时候，可以循环出来！
    Vue.component("todo-items",{
        props:["item","index"],
        template:"<li>{{index+1}},{{item}}</li>"
    });

    var vm = new Vue({
        el:"#vue",
        data:{
            title:"秦老师系列课程",
            todoItems:['test1','test2','test3']
        }
    });
</script>
```

## Vue自定义事件

- 组件如何才能删除 Vue 实例中的数据呢? 
- 此时就涉及到参数传递与事件分发了
- Vue 为我们提供了自定义事件的功能很好的帮助我们解决了这个问题
- 使用 this.$emit(‘自定义事件名’， 参数) 

```html
<body>
<!--view层，模板-->
<div id="vue">
    <todo>
        <todo-title slot="todo-title" :title="title_text"></todo-title>
        <todo-items slot="todo-items" v-for="(item,index) in todoItems"
                    :item_p="item" :index_p="index" v-on:remove="removeItems(index)" :key="index"></todo-items>
    </todo>
</div>
    
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.min.js"></script>
<script type="text/javascript">
    Vue.component('todo',{
        template:'<div>\
                <slot name="todo-title"></slot>\
                <ul>\
                    <slot name="todo-items"></slot>\
                </ul>\
            </div>'
    });
    Vue.component('todo-title',{
        props:['title'],
        template:'<div>{{title}}</div>'
    });
    //这里的index，就是数组的下标，使用for循环遍历的时候，可以循环出来！
    Vue.component("todo-items",{
        props:["item_p","index_p"],
        template:"<li>{{index_p+1}},{{item_p}} <button @click='remove_methods'>删除</button></li>",
        methods:{
            remove_methods:function (index) {
                //this.$emit 自定义事件分发
                this.$emit('remove',index);//remove是自定义事件名称
            }
        }
    });

    var vm = new Vue({
        el:"#vue",
        data:{
            title_text:"秦老师系列课程",
            todoItems:['test1','test2','test3']
        },
        methods:{
            removeItems:function(index){
                console.log("删除了"+this.todoItems[index]+"OK");
                this.todoItems.splice(index,1);
            }
        }
    });
</script>
```

# vue版本问题

[vue 和 vue/cli 版本对应](https://juejin.cn/post/7137120520234860558)

Vue CLI 的包名称由 vue-cli 改成了 @vue/cli。 如果你已经全局安装了旧版本的 vue-cli (1.x 或 2.x)，你需要先通过 npm uninstall vue-cli -g 或 yarn global remove vue-cli 卸载它。

再使用cnpm install -g @vue/cli命令

# npm常用命令

## npm build

- 对于web app而言，"build"的用处一般就是编译（如果有类似ts、scss代码）、打包、压缩、优化、移除注释、添加版权声明、拷贝资源等（看用了哪些plugins）。所以生产环境的web app都是得经过“build”后发布的，不“build”，那一般指的是开发模式，自己本地调试用，不能用于线上应用发布。

## npm install xxx

安装项目到项目目录下，不会将模块依赖写入`devDependencies`或`dependencies`。

## **npm install -g xxx**:

`-g`的意思是将模块安装到全局，不是安装到当前目录的项目下

## **npm install -save xxx**:

`--save` 等同于`-S` （常用，**可保存在package.json文件中**），-S, --save 安装包信息将加入到dependencies（生产阶段的依赖,也就是项目运行时的依赖，就是程序**上线后**仍然需要依赖）

## npm install -save-dev xxx:

`--save-dev` 等同于 `-D`，-D, --save-dev 安装包信息将加入到*dev*Dependencies（开发阶段的依赖，就是我们在开发过程中需要的依赖，只在开发阶段起作用。）

## npm cache clean --force

- 清除缓存





# 创建第一个Vue项目

- Vue 的开发都是要基于 NodeJS，实际开发采用 vue-cli 脚手架开发，vue-router 路由，vuex 做状态管理；Vue UI，界面一般使用 ElementUI（饿了么出品），或者 ICE（阿里巴巴出品）来快速搭建前端项目~~

## 安装Node.js

- http://nodejs.cn/download/
- cmd 下输入`node -v`，`npm -v`查看版本
- npm是一个软件包管理工具，和 apt 差不多

## 安装淘宝镜像加速器

- npm install cnpm -g
- -g：全局安装
- 虽然安装了 cnpm，但是尽量少用

## 安装vue-cli

- cnpm install vue-cli -g
- vue list：查看可以基于哪些模板创建vue应用程序，通常我们选择webpack

## 创建vue应用程序

1. vue init webpack myvue
   - 出现提示都选择no
2. cd myvue
3. npm install
4. npm run dev
   1. 出现问题，请参考[vue-cli安装与问题](https://blog.csdn.net/qdlxlxyzlxy/article/details/122965448)

## 创建方法2

- vue create project_name

# webpack

- webpack 是一个现代 JavaScript 应用程序的静态模块打包器 (module bundler) 。当 webpack 处理应用程序时， 它会递归地构建一个依赖关系图 (dependency graph) ， 其中包含应用程序需要的每个模块， 然后将所有这些模块打包成一个或多个 bundle.

## 安装

- npm install webpack -g
- npm install webpack-cli -g
- webpack -v
- webpack-cli -v

## 配置

- 创建 `webpack.config.js`配置文件
  - entry：入口文件， 指定 Web Pack 用哪个文件作为项目的入口
  - output：输出， 指定 WebPack 把处理完成的文件放置到指定路径
  - module：模块， 用于处理各种类型的文件
  - plugins：插件， 如：热更新、代码重用等
  - resolve：设置路径指向
  - watch：监听， 用于设置文件改动后直接打包

```js
module.exports = {
    entry: './modules/main.js',
    output: {
        filename: './js/bundle.js'
    }
};
```

## 打包

- 直接运行`webpack`命令打包

# vue-router

- Vue Router 是 Vue.js 官方的路由管理器。它和 Vue.js 的核心深度集成，让构建单页面应用变得易如反掌
- vue-router 是一个插件包， 所以还是需要用 npm/cnpm 来安装
- `npm install vue-router --save-dev`

## vue-router路由配置

```js
//此处省略各种import
Vue.use(Router);

export default new Router({
  mode: 'history',//两种路由模式：hash，路径带#符号；history，路径不带#符号
  routes: [
    {
      path: '/login',
      component: Login
    },
    {
      path: '/main/:name',
      component: Main,
      props: true,
      children: [ //children表示嵌套路由
        {
          path: '/user/profile/:id/:name',
          name: 'UserProfile',
          component: UserProfile,
          props: true
        },
        {
          path: '/user/list',
          component: UserList
        }
      ]
    },
    {
      path: '/goHome',
      redirect: '/login' //访问goHome会重定向到/login
    },
    {
      path: '*',//配置404路由，只要匹配不到的url都会跳到NotFound组件
      component: NotFound
    }
  ]
})
```

## router-link和router-view

- router-view 主要是在构建单页应用时，方便渲染指定路由对应的组件，渲染的组件是由vue-router指定的。
- router-link 组件支持用户在具有路由功能的应用中 (点击) 导航

```vue
<template>
  <div id="app">
    <h1>Vue-Router</h1>
    <router-link to="/main">首页</router-link>
    <router-link to="/content">内容页</router-link>
    <router-view></router-view>
  </div>
</template>
```

## 嵌套路由

- 嵌套路由又称子路由，通常由多层嵌套的组件组合而成
- 比如在一个页面中， 在页面的上半部分，有三个按钮，而下半部分是根据点击不同的按钮来显示不同的内容，那么我们就可以在这个组件中的下半部分看成是一个嵌套路由，也就是说在这个组件的下面需要再来一个<router-view>， 当我点击不同的按钮时，他们的<router-link>分别所指向的组件就会被渲染到这个<router-view>中

## 通过路由传递参数

```vue
<router-link :to="{name:'UserProfile',params:{id:1,name:'狂神'}}">个人信息</router-link>
```

路由配置文件：

```js
routes: [
    {
      	path: '/user/profile/:id/:name',
        name: 'UserProfile',
        component: UserProfile,
        props: true
    },
]
```

取出参数：

```vue
<template>
    <div>
      <h1>个人信息</h1>
      {{$route.params.id}}
      {{$route.params.name}} <!-- 通过路由传递参数，第1种方式 -->
      {{id}} <!-- 第2种方式 -->
      {{name}}
    </div>
</template>

<script>
    export default {
      name: "UserProFile",
      props: ['id','name'] //第2种方式
    }
</script>
```



## 路由钩子

- beforeRouteEnter：在进入路由前执行
- beforeRouteLeave：在离开路由前执行
- 参数说明：
  - to：路由将要跳转的路径信息
  - from：路径跳转前的路径信息
  - next：路由的控制参数
    - next() 跳入下一个页面
    - next(’/path’) 改变路由的跳转方向，使其跳到另一个路由
    - next(false) 返回原来的页面
    - next((vm)=>{}) 仅在 beforeRouteEnter 中可用，vm 是组件实例
- 注：static下的资源能直接被浏览器访问

```vue
<script>
    export default {
      name: "UserProFile",
      props: ['id','name'],
      beforeRouteEnter: (to,from,next) => {
        console.log('进入路由之前');//加载数据
        next(vm => {
          vm.getData();//进入路由之前执行getData()
        })
      },
      beforeRouteLeave: (to,from,next) => {
        console.log('进入路由之后');
        next()
      },
      methods: {
        getData() {
          this.axios({
            method: 'get',
            url: 'http://localhost:8080/static/mock/data.json'
          }).then((response) => {
            console.log(response)
          })
        }
      }
    }
</script>
```

# vuex

- 是专门为 Vue 开发的状态管理方案，我们可以把需要在各个组件中传递使用的变量、方法定义在这里
- 安装：cnpm install vuex@3.1.0 --save

# 版本问题

- 使用npm时遇到一些版本问题，可以直接在package.json里面改，然后直接把node_modules文件夹删掉，重新cnpm install
- node.js版本也可能导致一些问题，node.js和node-sass版本对应关系如下

<img src="./resource/2343289jfesijfisdj.png" alt="在这里插入图片描述" style="zoom:80%;" />

# Vue项目结构

- assets：用于存放资源文件
- components：用于存放 Vue 功能组件（最常修改的部分就是 components 文件夹了，几乎所有需要手动编写的代码都在其中）
- views：用于存放 Vue 视图组件
- router：用于存放 vue-router 配置

---

- [关于Vue中main.js，App.vue，index.html之间关系进行总结](https://www.cnblogs.com/chenleideblog/p/10484554.html)

<img src="./resource/vue.md" alt="image-20230817185009366" style="zoom:50%;" />

# ElementUI

> [官网](https://element.eleme.cn/#/zh-CN)

# 其他问题

## export default 是什么

1. 声明一个 vue，相当于 new Vue({}) 
2. 达到可复用的目的，也就是说，export default 相当于导出当前 vue 组件，在其它引入当前组件时可以使用当前组件中的方法和变量。

## export default{ data () 是什么

- 起到局部变量的作用。也就是说，这个 data（）中 return 的变量和方法只限于当前声明此 data（）的组件使用

## 在引入组件时路径上加上 @符作用是什么？

- @ 等价于 /src 这个目录，避免写麻烦又易错的相对路径

## mount函数

Vue 官方文档中对 mounted 的描述如下：

**el 被新创建的 vm.$el 替换，并挂载到实例上去之后调用该钩子。**

我们 new 的这个 Vue 对象，就是 `vm.$el` ，而 `el` ，则是 `index.html` 里的一个 div。

除了 mounted 之外，也可以使用 created，但把初始化的操作放在 created 里可能会导致渲染变慢，但差别并不是很大（据说在低端 Android 机上能看出来）。

## ref 特性

- 用来给元素或子组件注册引用信息。引用信息将会注册在父组件的 `$refs` 对象上。如果在普通的 DOM 元素上使用，引用指向的就是 DOM 元素；如果用在子组件上，引用就指向组件实例。
- 通俗理解：也就是说给子组件或者元素添加一个引用标识，而这个标识就会被记录

## this.$ 是什么

- $ 是在 Vue 所有实例中都可用的属性的一个简单约定

## Vue.prototype

- 你可能会在很多组件里用到数据/实用工具，但是不想污染全局作用域。这种情况下，你可以通过在原型上定义它们使其在每个 Vue 的实例中可用
- Vue.prototype.$appName = 'test'

## Vue.use()

- 用于安装插件
- 该方法需要在调用 `new Vue()` 之前被调用
- 内部会调用install方法，install会做很多事，比如在Vue原型上注册一些方法，这就是为什么我们可以直接使用this.$alert、this.$loading的原因、比如注册`<router-link>`、`<router-view>`组件
- 参考<https://juejin.cn/post/6844903946343940104>
