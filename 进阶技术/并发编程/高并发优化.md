# 接口高并发

- 一个接口，理论的最佳响应速度应该在200ms以内，或者慢点的接口就几百毫秒。
- 如果一个接口响应时间达到1秒+，建议考虑用缓存、索引、NoSQL等各种你能想到的技术手段，优化一下性能。否则你要是胡乱设置超时时间是几秒，甚至几十秒，万一下游服务偶然出了点问题响应时间长了点呢？那你这个线程池里的线程立马全部卡死！

# 服务器高并发

## "too many open files" 该如何解决

- 每打开一个文件（包括socket），都需要消耗一定的内存资源。为了避免个别进程不受控制的打开了过多文件而让整个服务器奔溃，Linux对打开的文件描述符数量有限制。如果你的进程触发到内核的限制，那么"too many open files" 报错就产生了

- 可以通过修改这三个参数的值来修改进程能打开的最大文件描述符数量

  ```
  fs.file-max 
  soft nofile
  fs.nr_open
  ```

- 需要注意这三个参数之间的耦合关系！

## 一台服务端机器最大究竟能支持多少条连接

因为这里要考虑的是最大数，因此先不考虑连接上的数据收发和处理，仅考虑`ESTABLISH`状态的空连接。那么一台服务端机器上最大可以支持多少条TCP连接？这个连接数会受哪些因素的影响？

- 在不考虑连接上数据的收发和处理的情况下，仅考虑`ESTABLISH状态`下的空连接情况下，一台服务器上最大可支持的TCP连接数量基本上可以说是由内存大小来决定的。
- 四元组唯一确定一条连接，但服务端可以接收来自任意客户端的请求，所以根据这个理论计算出来的数字太大，没有实际意义。另外文件描述符限制其实也是内核为了防止某些应用程序不受限制的打开【`文件句柄`】而添加的限制。这个限制只要修改几个内核参数就可以加大。
- 一个socket大约消耗3kb左右的内存，这样真正制约服务端机器最大并发数的就是内存，拿一台4GB内存的服务器来说，可以支持的TCP连接数量大约是100w+

## 一个客户端机器最大究竟能支持多少条连接

和服务端不同的是，客户端每次建立一条连接都需要消耗一个端口。在TCP协议中，端口是一个2字节的整数，因此范围只能是0~65535。那么客户单最大只能支持65535条连接吗？有没有办法突破这个限制，有的话有哪些办法？

- 客户度每次建立一条连接都需要消耗一个端口。从数字上来看，似乎最多只能建立65535条连接。但实际上我们有两种办法破除65535这个限制
  - 方式一，为客户端配置多IP
  - 方式二，分别连接不同的服务端
- 所以一台client发起百万条连接是没有任何问题的

# 参考

https://juejin.cn/post/7162824884597293086#heading-7

**参考书籍**：《深入理解Linux网络》

# 其他

必须吐槽一下！一个高并发项目开发到落地的心酸路。https://mp.weixin.qq.com/s/Ou8h9YKblKl7ncf95grEvg