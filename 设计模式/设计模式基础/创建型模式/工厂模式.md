# 3种工厂模式

- 简单工厂：一个工厂，通过type类型创建不同产品
- 工厂方法：每个产品一个工厂
- 抽象工厂：每个产品组合一个工厂

总的来说**用简单工厂模式比较多**，工厂方式模式的话代码量会比较大，抽象工厂模式的话需要业务比较大的情况下才会用到

# 简单工厂模式

## 简单工厂模式的构成

- 具体的工厂角色：Factory；在工厂中可以调用方法生产出具体的某个类型的产品；
- 抽象的产品角色：Product；在抽象产品类型中可以声明抽象接口，在具体的产品类中实现；
- 具体的产品角色：ProductA 和ProdcutB；在具体的类中必须实现抽象类的接口，也可以实现自己的业务逻辑。

## 简单工厂模式的优点与缺点

- 优点：客户端与产品的创建分离，客户端不需要知道产品创建的逻辑，只需要消费该产品即可。
- 缺点：工厂类集成了所有产品的创建逻辑，当工厂类出现问题，所有产品都会出现问题；还有当新增加产品都会修改工厂类，违背开闭原则

## Java实现

```java
//抽象产品类
public abstract class Prodcut {
    //抽象产品方法
    public abstract sayHi();
}

//继承抽象类实现产品A类
public class ProductA extends Product {
    
    //实现抽象产品方法
    @Overide
    public abstract sayHi(){
        System.out.println("Hi, I'm ProductA");
    }
}

//继承抽象类实现产品A类
public class ProductB extends Product {
    
    //实现抽象产品方法
    @Overide
    public abstract sayHi(){
        System.out.println("Hi, I'm ProductB");
    }
}

public class Factory {
    
    public Factory() {
    }
    
    //第一种写法
    //根据产品名称生产产品
    public Product createProduct(String productName) {
        Product product = NULL;
        switch(productName){
            case "A":
                product = new ProductA();//生产产品A
                break;     
            case "B":
                product = new ProductB();//生产产品B
                break;
            default:
                break;  
        }
        return product;
    }
    
    //第二种写法
    //生产产品A
    public ProductA createProductA() {
        return new ProductA();//生产产品A
    }
    
    //生产产品B
    public ProductB createProductB() {
        return new ProductB();//生产产品B
    }
}
```

# 工厂方法模式

## 工厂方法模式的构成

- 抽象产品类：Product；描述具体产品的公共接口，在具体的产品类中实现
- 具体产品类：ProductA和ProductB；具体产品类，实现抽象产品类的接口，工厂类创建对象
- 抽象工厂类：Factory；描述具体工厂的公共接口
- 具体工厂类：FactoryA和FactoryB；描述具体工厂类，实现创建产品类对象，实现抽象工厂类的接口

## 工厂方法模式的优缺点

- 优点：解决简单工厂模式的弊端，更符合开闭原则，增加一个产品类，则只需要实现其他具体的产品类和具体的工厂类即可；符合单一职责原则，每个工厂只负责生产对应的产品
- 缺点：增加一个产品，就需要实现对应的具体工厂类和具体产品类；每个产品需要有对应的具体工厂和具体产品类

## Java实现

```java
//抽象产品类
public abstract class Prodcut {
    //抽象产品方法
    public abstract sayHi();
}

//继承抽象类实现产品A类
public class ProductA extends Product {
    
    //实现抽象产品方法
    @Overide
    public abstract sayHi(){
        System.out.println("Hi, I'm ProductA");
    }
}

//继承抽象类实现产品A类
public class ProductB extends Product {
    
    //实现抽象产品方法
    @Overide
    public abstract sayHi(){
        System.out.println("Hi, I'm ProductB");
    }
}

//工厂抽象类
public abstract class Factory {
    //抽象工厂方法
    public abstract Product createProduct();
}

//具体工厂类FactoryA
public class FactoryA extends Factory {
    
    @Overide
    public Product createProduct() {
        System.out.println("生产了一个A");
        return new ProductA();//生产产品A
    }
}

//具体工厂类FactoryB
public class FactoryB extends Factory {
    
    @Overide
    public Product createProduct() {
        System.out.println("生产了一个B");
        return new ProductB();//生产产品B
    }
}
```

# 抽象工厂模式

- 抽象工厂模式和工厂方法模式的区别就在于产品之间不再是彼此毫无关联，而是有多组产品，每组产品有其共性（可称为一个产品族），且不同组的产品可以根据需求相互组合成一个合体产品
- 最简单的区别方法就是抽象工厂模式的工厂接口中不止一个方法

## 抽象工厂模式的构成

- 抽象工厂角色：是具体工厂角色必须实现的接口或者必须继承的父类。
- 具体工厂角色：继承抽象工厂类，实现创建对应的具体产品的对象。
- 抽象产品角色：它是具体产品继承的父类或者是实现的接口。这里抽象产品至少2个维度；
- 具体产品角色：具体工厂角色所创建的对象就是此角色的实例。

## UML图

![image-20230713003302268](./resource/image-20230713003302268.png)

## 抽象工厂模式的优缺点（了解）

- 优点：1.当一个产品族中的多个对象被设计成一起工作时，**它能保证客户端始终只使用同一个产品族中的对象**。2.保证减少工厂类和具体产品的类添加。
- 缺点：产品族扩展非常困难，要增加一个系列的某一产品，既要在抽象的工厂和抽象产品里加代码，又要在具体的里面加代码。

## Java实现

```java
//抽象形状类
public abstract class Shape {
    public abstract void sayShape();
}

//矩形类
public class Retangle extends Shape {
    
    @Overide
    public void sayShape() {
        System.out.println("Hi, I'm Retangle!");
    }
}

//圆形类
public class Circle extends Shape {

    @Overide
    public void sayShape() {
        System.out.println("Hi, I'm Circle!");
    }
}

//抽象颜色类
public abstract class Color {
    public abstract void sayColor();
}

//红色类
public class Red extends Color {
    @Overide
    public void sayColor() {
        System.out.println("Hi, I'm Red!");
    }
}

//白色类
public class White extends Color {
    @Overide
    public void sayColor() {
        System.out.println("Hi, I'm White!");
    }
}

//抽象工厂类
public abstract class ShapeFactory {

    public abstract Shape createShape();
    public abstract Color createColor();
}

//红色的圆
public class RedCircleFactory extends ShapeFactory {

    @Overide
    public Shape createShape() {
        return new Circle();
    }
    
    @Overide
    public Color createColor() {
        return new Red();
    }
}

//白色的圆
public class WhiteCircleFactory extends ShapeFactory {

    @Overide
    public Shape createShape() {
        return new Circle();
    }
    
    @Overide
    public Color createColor() {
        return new White();
    }
}
```

