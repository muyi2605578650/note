[toc]

## 依赖

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-consul-discovery</artifactId>
</dependency>
```



## 配置文件

```yaml
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        service-name: ${spring.application.name}
    loadbalancer:
      ribbon:
        enabled: false
```

## 文档

- [文档](https://www.springcloud.cc/spring-cloud-consul.html)

