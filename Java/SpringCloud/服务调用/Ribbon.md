[toc]

# Spring Cloud Ribbon概述

- Spring Cloud Ribbon是一个基于HTTP和TCP的客户端负载均衡工具，它基于`Netflix Ribbon`实现。通过Spring Cloud的封装，可以让我们轻松地将面向服务的REST模版请求自动转换成客户端负载均衡的服务调用。
- Spring Cloud Ribbon只是一个工具类框架，不像服务注册中心、配置中心、API网关那样需要独立部署，但是它几乎存在于每一个Spring Cloud构建的微服务和基础设施中。因为微服务间的调用，API网关的请求转发等内容，实际上都是通过Ribbon来实现的，包括`Feign`也是基于Ribbon实现的工具。
- 一句话：ribbon就是`负载均衡+RestTemplate调用`
- 现在Ribbon已经`停止维护`，但是还是有不少人使用

# 目前主流的负载均衡方案

## 集中式负载均衡

- 即在consumer和provider之间使用独立的负载均衡设施(可以是硬件，如`F5`, 也可以是软件，如`nginx`), 由该设施负责把访问请求通过某种策略转发至provider

## 进程内负载均衡

- 将负载均衡逻辑集成到consumer，consumer从服务注册中心获知有哪些地址可用，然后自己再从这些地址中选择出一个合适的provider

Ribbon就属于后者，它只是一个类库，集成于consumer进程，consumer通过它来获取到provider的地址。

# 软件负载均衡和硬件负载均衡

- [浅谈软件和硬件负载均衡（LVS、HAProxy、Nginx、F5）及一次线上问题分析](https://zhuanlan.zhihu.com/p/103107389)

# Ribbon的核心组件IRule

- Ribbon默认的负载均衡算法是`轮询`(原理是取模服务总数)，而IRule是根据特定的算法中从服务列表中选取其中一个要访问的服务。

![image-20200905163732232](../resource/format.png)

## Ribbon配置随机负载均衡

添加依赖，ribbon的依赖一般都集成在了starter里面

- 在启动类所在包的`同级目录`下创建一个包，然后新建一个类

```java
@Configuration
public class MySelfRule
{
    @Bean
    public IRule myRule()
    {
        return new RandomRule();//定义为随机
    }
}
```

- 启动类加一行注解: @RibbonClient(name = "CLOUD-PAYMENT-SERVICE",configuration=MySelfRule.class)
- name是指被调用的服务名称

```java
@SpringBootApplication
@EnableEurekaClient
@RibbonClient(name = "CLOUD-PAYMENT-SERVICE", configuration=MySelfRule.class)
public class OrderMain80
{
    public static void main(String[] args) {
        SpringApplication.run(OrderMain80.class, args);
    }
}
```

## 随机负载均衡底层核心代码

```java
public Server choose(ILoadBalancer lb, Object key) {
        if (lb == null) {
            log.warn("no load balancer");
            return null;
        } else {
            Server server = null;
            int count = 0;
            while(true) {
                if (server == null && count++ < 10) {
                    List<Server> reachableServers = lb.getReachableServers();
                    List<Server> allServers = lb.getAllServers();
                    int upCount = reachableServers.size();
                    int serverCount = allServers.size();
                    if (upCount != 0 && serverCount != 0) {
                        int nextServerIndex = this.incrementAndGetModulo(serverCount);//获取索引
                        server = (Server)allServers.get(nextServerIndex);//获取server
                        if (server == null) {
                            Thread.yield();
                        } else {
                            if (server.isAlive() && server.isReadyToServe()) {
                                return server;
                            }
                            server = null;
                        }
                        continue;
                    }
                    log.warn("No up servers available from load balancer: " + lb);
                    return null;
                }
                if (count >= 10) {
                    log.warn("No available alive servers after 10 tries from load balancer: " + lb);
                }
                return server;
            }
        }
    }

    private int incrementAndGetModulo(int modulo) {
        int current;
        int next;
        do {//cas自旋
            current = this.nextServerCyclicCounter.get();
            next = (current + 1) % modulo;
        } while(!this.nextServerCyclicCounter.compareAndSet(current, next));

        return next;
    }
```

