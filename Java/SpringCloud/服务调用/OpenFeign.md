[toc]

# Open Feign简介

- Feign是一个`声明式WebService客户端`。使用Feign能让编写WebService客户端更加简单。
- Feign 最早是由 Netflix 公司进行维护的，后来 Netflix 不再对其进行维护，最终 Feign 由社区进行维护，更名为 Openfeign
- 使用方法：定义一个服务接口然后在上面添加注解。Feign也能支持`可拔插式`的`编码器`和`解码器`。
- Spring Cloud对Feign进行了封装，使其能够支持SpringMVC标准注解和HttpMessageConverters。Feign可以与Eureka和`Ribbon`组合使用完成负载均衡。
- Feign旨在使Java Http客户端变得更加容易。在使用Ribbon+RestTemplate时，往往一个服务提供接口会被多处调用。所以，Feign在此基础上做了进一步的封装，由他来帮助我们定义和实现接口。在Feign的实现下，我们只需要使用注解配置接口，即可完成对服务提供方的接口绑定，`简化了自己封装服务调用的工作量`。

# 配置和使用

## 依赖

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
```

## 启动类

```java
@SpringBootApplication
@EnableFeignClients
public class OrderFeignMain80
{
    public static void main(String[] args) {
            SpringApplication.run(OrderFeignMain80.class, args);
    }
}
```

## 封装接口

```java
@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")//被调用的服务名
public interface PaymentFeignService
{
    @GetMapping(value = "/payment/get/{id}")//被调用的接口地址
    CommonResult<Payment> getPaymentById(@PathVariable("id") Long id);

    @GetMapping(value = "/payment/feign/timeout")
    String paymentFeignTimeout();
}
```

## 超时配置

- feign请求服务接口默认只等待1s

```yaml
server:
  port: 80
eureka:
  client:
    register-with-eureka: false
    service-url:
      defaultZone: http://eureka7001.com:7001/eureka/,http://eureka7002.com:7002/eureka/
#设置feign客户端超时时间(OpenFeign默认支持ribbon)
ribbon:
#指的是建立连接后从服务器读取到可用资源所用的时间
  ReadTimeout: 5000
#指的是建立连接所用的时间，适用于网络状况正常的情况下,两端连接所用的时间
  ConnectTimeout: 5000

logging:
  level:
    # feign日志以什么级别监控哪个接口
    com.atguigu.springcloud.service.PaymentFeignService: debug
```

## 日志增强级别

```java
@Configuration
public class FeignConfig
{
    @Bean
    Logger.Level feignLoggerLevel()
    {
        return Logger.Level.FULL;//FULL是最详细的级别
    }
}
```

# 自定义负载均衡

- 和ribbon的配置一样（需要引入相关的配置类和注解）