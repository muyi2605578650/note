[toc]



# Spring Cloud Stream简介

- 是一个构建消息驱动微服务的框架。屏蔽了消息中间件之间的差异，统一了消息的编程模型（有点类似于JDBC）
- 应用程序通过 inputs 或者 outputs 来与 Spring Cloud Stream 中 **binder 对象**交互
- 通过我们配置来 binding(绑定) ，而 Spring Cloud Stream 的 binder 对象负责与消息中间件交互。所以，我们只需要搞清楚如何与 Spring Cloud Stream 交互就可以了，不需要对所有的消息中间件都很熟悉。
- Stream 中的消息通信方式遵循了`发布-订阅`模式
- 目前仅支持RabbitMQ、Kafka
- 参考：[参考](https://blog.csdn.net/weixin_44449838/article/details/111300096)

## 标准流程

<img src="./resource/image-20211207211936037.png" alt="image-20211207211936037" style="zoom: 33%;" />

## 为什么要引入 SpringCloud Stream

- 中间件的差异性导致我们实际项目开发给我们造成了一定的困扰，我们如果用了两个消息队列的其中一种，后面的业务需求，我想往另外一种消息队列进行迁移，这时候无疑就是一个灾难性的，一大堆东西都要重新推倒重新做，因为它跟我们的系统耦合了，这时候 Stream 给我们提供了一种`解耦合`的方式。

## pom依赖

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-stream-rabbit</artifactId>
</dependency>
```

# Stream生产者相关代码

## 核心配置

```yaml
spring:
  application:
    name: cloud-stream-provider
  cloud:
      stream:
        binders: # 在此处配置要绑定的rabbitmq的服务信息；
          defaultRabbit: # 表示定义的名称，用于于binding整合
            type: rabbit # 消息组件类型
            environment: # 设置rabbitmq的相关的环境配置
              spring:
                rabbitmq:
                  host: localhost
                  port: 5672
                  username: guest
                  password: guest
        bindings: # 服务的整合处理
          output: # 这个名字是一个通道的名称
            destination: studyExchange # 表示要使用的Exchange名称定义
            content-type: application/json # 设置消息类型，本次为json，文本则设置“text/plain”
            binder: defaultRabbit # 设置要绑定的消息服务的具体设置
            group: atguiguA # 配置分组名
```

## 核心代码

```java
@EnableBinding(Source.class) //定义消息的推送管道
public class MessageProviderImpl implements IMessageProvider
{
    @Resource
    private MessageChannel output; // 消息发送管道

    @Override
    public String send()
    {
        String serial = UUID.randomUUID().toString();
        output.send(MessageBuilder.withPayload(serial).build());
        System.out.println("*****serial: "+serial);
        return null;
    }
}
```

# Stream消费者相关代码

## 核心配置（同生产者）

## 核心代码

```java
@Component
@EnableBinding(Sink.class)
public class ReceiveMessageListenerController
{
    @Value("${server.port}")
    private String serverPort;

    @StreamListener(Sink.INPUT)
    public void input(Message<String> message)
    {
        System.out.println("消费者,----->接受到的消息: "+message.getPayload()+"\t  port: "+serverPort);
    }
}
```

# 重复消费的问题

- Stream中同一个group中的多个消费者是竞争关系，能够保证消息只会被一个应用消费。而不同组可以重复消费
- 要将多个微服务分到同一组，只需要`将配置文件中分组名改为一样`就可以了

# 消息持久化

- 当加上 Group 配置后，就自动支持持久化功能了，即：如果消费者服务停了，一段时间后重启，仍会收到这段时间未接收的数据

