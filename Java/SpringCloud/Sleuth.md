[toc]

# Spring Cloud Sleuth简介

- 在微服务框架中，一个由客户端发起的请求在后端系统中会经过多个不同的的服务节点调用来协同产生最后的请求结果，形成─条复杂的分布式服务调用链路，链路中的任何一环出现高延时或错误都会引起整个请求最后的失败。
- Sleuth提供了一套完整的`服务调用链路跟踪`的解决方案
- **sleuth是对zipkin的封装**，对应Span,Trace等信息的生成
- 参考：[分布式链路跟踪Sleuth与Zipkin](https://blog.csdn.net/k_young1997/article/details/104239086)

# 如何使用

- 在需要被跟踪的微服务中添加如下依赖和配置

## pom依赖

```xml
<!--包含了sleuth+zipkin-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-zipkin</artifactId>
</dependency>
```

## 核心配置

```yaml
spring:
  application:
    name: cloud-payment-service
  zipkin:
    base-url: http://localhost:9411 # 配置zipkin 仪表盘的地址
  sleuth:
    sampler:
      probability: 1 #采样率值介于 0 到 1 之间，1 则表示全部采集
```

- 启动服务后访问配置的zipkin地址即可看到服务接口调用的链路情况