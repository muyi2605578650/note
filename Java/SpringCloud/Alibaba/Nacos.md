[toc]

# Nacos简介

- Nacos是Spring Cloud Alibaba的组件
- 全称：Dynamic `Na`ming and `Co`nfiguration `S`ervice（动态命名和配置服务）
- 简单一句话：Nacos=Eureka+Config+Bus


# Nacos安装

- 下载完成后直接运行bin目录下的startup.cmd，然后访问http://ip:8848/nacos登录，默认账户密码nacos
- standalone模式启动：startup.cmd -m standalone

# Nacos服务注册

## pom

```xml
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
</dependency>
```

## 启动类加@EnableDiscoveryClient

## 配置文件

```yaml
spring:
  application:
    name: nacos-payment-provider
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848 #配置Nacos地址

management:
  endpoints:
    web:
      exposure:
        include: '*' #暴露以便监控
```

# Nacos服务配置

## pom

```xml
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
</dependency>
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
</dependency>
```

## 配置文件

- 注意文件名叫`bootstrap.yml`（实际上可以有两个配置文件，一个bootstrap用于加载高优先级的配置，一个application用于加载本地配置）

```yaml
spring:
  profiles:
    active:
      dev
  application:
    name: nacos-config-client
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848 #Nacos服务注册中心地址
      config:
        server-addr: localhost:8848 #Nacos作为配置中心地址
        file-extension: yaml #指定yaml格式的配置
        group: DEV_GROUP #配置文件分组名
        namespace: 7d8f0f5a-6a53-4785-9686-dd460158e5d4
```

## 加注解

- 在读取配置文件的类上加`@RefreshScope`

```java
@RestController
@RefreshScope //支持Nacos的动态刷新功能。
public class ConfigClientController
{
    @Value("${config.info}")
    private String configInfo;

    @GetMapping("/config/info")
    public String getConfigInfo() {
        return configInfo;
    }
}
```

## 配置中心创建配置文件

- 每个配置文件有个`DataId`，其格式为：`${prefix}-${spring.profiles.active}.${file-extension}`
  - prefix默认为`spring.application.name`的值
  - file-extension为`spring.cloud.nacos.config.file-extension`的值
  - 如：nacos-config-client-dev.yaml

如图所示：

<img src="../resource/image-nacos-config.png" alt="image-20211214214444582" style="zoom: 50%;" />

## Namespace、Group、DataId的关系

- Namespace一般用于区分部署环境（开发、测试、生产），Group用于把不同的微服务划分到同一组
- 当然也可以按自己的规则来，比如根据DataId或Group区分部署环境（比如在nacos上同一个分组创建nacos-config-client-dev和nacos-config-client-prod，同时服务中切换对应的配置，就能实时读取。或者创建不同分组，修改spring.cloud.nacos.config.group的值也可以），因为服务少且简单的情况下没必要配什么Namespace
- 也有这样的：Namespace对应企业，Group对应项目，DataID对应环境
- 默认情况下，Namespace=public，Group=DEFAULT_GROUP，cluster=DEFAULT

<img src="../resource/image-20211214214945758.png" alt="image-20211214214945758" style="zoom:50%;" />

# Nacos支持AP和CP模式的切换

- 一般来说，如果不需要存储服务级别的信息且服务实例是通过nacos-client注册，并能够保持心跳上报，那么就可以选择AP模式。当前主流的服务如 Sping cloud和Dubbo服务，都适用于AP模式，AP模式为了服务的可能性而减弱了一致性，因此AP模式下只支持注册临时实例。
- 如果需要在服务级别编辑或者存储配置信息，那么CP是必须，K8S服务和DNS服务则适用于CP模式。CP模式下则支持注册持久化实例，此时则是以Raft协议为集群运行模式，该模式下注册实例之前必须先注册服务，如果服务不存在，则会返回错误。

#  Nacos集群

- 官网的集群架构图如下，VIP是指虚拟IP，即nginx（集群）

<img src="../resource/image-20220110104355314.png" alt="image-20220110104355314" style="zoom: 33%;" />

<img src="../resource/image-20220414171430183.png" alt="image-20220414171430183" style="zoom: 50%;" />

## Nacos的数据存储

- 重启 Nacos 后，之前配置的信息还都存在，因为Nacos 默认使用嵌入式数据库（derby）实现数据的存储。
- 如果启动多个默认配置下的 Nacos 节点，数据存储是存在一致性问题的。
- 为了解决这个问题，Nacos 采用了集中式存储的方式来支持集群化部署，目前只支持 MySQL 的存储

![image-20220216105804345](../resource/image-20220216105804345.png)

## Linux版Nacos集群+MySQL生产环境配置

- 注意：至少3个nacos才能构成集群
- Linux上需要安装好Nacos、MySQL、nginx
- 具体很简单，[参考](https://blog.csdn.net/weixin_44449838/article/details/111405082)，但这篇文章是在单机上搭建的nacos集群，属于伪集群