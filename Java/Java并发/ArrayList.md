# ArrayList在迭代时产生并发修改异常的原因

ArrayList在迭代时，不可以通过集合对象的方法操作集合中的元素。会发生ConcurrentModeificationException异常，在使用迭代器时，只能用迭代器的方法操作元素

## 异常演示

```java
List<String> list = new ArrayList<String>();
list.add("a");
list.add("b");
list.add("c");
list.add("d");
list.add("e");
// list.add("d");
Iterator<String> iterator = list.iterator();
while (iterator.hasNext()) {
    String str = iterator.next();
    if (str.equals("a")) {
        // list.remove(str);
        iterator.remove();
    } else {
        System.out.println(str);
    }
}
```

## 源码分析



## 为什么要有ListIterator

- 在迭代时，不可以通过集合对象的方法操作集合中的元素。会发生ConcurrentModeificationException异常，在使用迭代器时，只能用迭代器的方法操作元素，但它的方法只有3个，所以提供了ListIterator

# 参考

[详解ArrayList在遍历时remove元素所发生的并发修改异常的原因及解决方法](https://blog.csdn.net/weixin_44463178/article/details/108838938)