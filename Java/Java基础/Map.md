## 为什么要重写hashcode和equals

- 
  hashmap的元素比较是这样的：先hashcode比较，如果不同再equals比较
- 如果不重写hashcode方法，默认就比较对象的地址值；同样，如果不重写equals方法，默认也是比较对象的地址值。这样的话，假设新增两个一样的对象，会被判定为不相同（因为地址不一样），被重复插入（索引不同），同一个map中就存在2个在业务上相同的对象了
- 假设只重写了hashcode没重写equals，因为最终equals也是根据对象地址判断，所以还是判定为不相同，所以被重复插入（索引相同的位置）
- 假设重写了equals没重写hashcode，那更简单了，因为会先比较hashcode，所以被重复插入（索引不同）

## 举例：创建一个学生类所要做的事

1. 覆写toString、equals
2. 实现comparable，覆写compareTo
3. 覆写hashcode

```java
class Stu implements Comparable<Stu>{
    private int age;
    private String name;
    Stu(int age, String name){
        this.age = age;
        this.name = name;
    }
    
    public int compareTo(Stu s){//age主要排序条件
        int num = new Integer(this.age).compareTo(new Integer(s.age));
        if(num == 0){
            return this.name.compareTo(s.name);
        }
        return num;
	}
	
    public int hashCode(){
		return name.hashCode() + age*34;
	}
	
    public boolean equals(Object obj){
		if(!(obj instanceof Stu))
			throw new ClassCastException("type error");
		Stu s = (Stu)obj;
		return this.name.equals(s.name) && this.age == s.age;
	}
    
	public String toString(){
		return name + ":" + age;
	}
}
```