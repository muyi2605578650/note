
# Lambda表达式与函数式接口

- 主要用于`简化创建匿名内部类对象`，它可以实现抽象方法的方法体
- 但lambda和一般的匿名内部类有区别：
  - `lambda只适用于函数式接口，匿名内部类适用于任意接口、抽象类、甚至普通类`
- 可以理解为lambda是匿名内部类的一个`子集`

---

- 函数式接口在Java中是指：`有且仅有一个抽象方法的接口` 
- Java中的函数式编程体现就是Lambda，所以函数式接口就是可以适用于Lambda使用的接口 

## 语法糖

- “语法糖”是指使用更加方便，但是原理不变的代码语法。
- 例如在遍历集合时使用的for-each语法，其实底层的实现原理仍然是迭代器，这便是“语法糖”。
- 从应用层面来讲，Java中的Lambda可以被当做是匿名内部类的“语法糖”，但是二者在原理上是不同的 

## 函数式接口的好处 -todo

- 给程序带来性能优化，参考：https://blog.csdn.net/weixin_38214171/article/details/85081759
- 简化代码

## 方法引用

- `::`为引用运算符，而它所在的表达式被称为方法引用
- 如果Lambda要表达的函数方案`已经存在于某个方法的实现中`，那么则可以通过双冒号来引用该方法作为Lambda的替代者（demo01里也有示例）

```java
/*
    定义一个打印的函数式接口
 */
@FunctionalInterface
public interface Printable {
    //定义字符串的抽象方法
    void print(String s);
}

public class Demo01Printable {
    //定义一个方法,参数传递Printable接口,对字符串进行打印
    public static void printString(Printable p) {
        p.print("HelloWorld");
    }
    public static void main(String[] args) {
        //调用printString方法,方法的参数Printable是一个函数式接口,所以可以传递Lambda
        printString((s) -> {
            System.out.println(s);
        });
        /*
            分析:
                Lambda表达式的目的,打印参数传递的字符串
                把参数s,传递给了System.out对象,调用out对象中的方法println对字符串进行了输出
                注意:
                    1.System.out对象是已经存在的
                    2.println方法也是已经存在的
                所以我们可以使用方法引用来优化Lambda表达式
                可以使用System.out方法直接引用(调用)println方法
         */
        printString(System.out::println);
    }
}
```

## 函数式接口练习Demo01

```java
public class FunctionalInterfaceDemo {
    public static void test(FunInterface1 funInterface1) {
        funInterface1.say();
    }

    public static void main(String[] args) {
        //1. 参数：传函数式接口的实现类
        test(new FunInterface1Impl());

        //2. 参数：new一个实现类（不使用lambda）
        test(new FunInterface1() {
            @Override
            public void say() {
                System.out.println("use new");
                System.out.println("use new...");
            }
        });

        //3. 参数：new一个实现类（使用lambda）
        test(() -> {
            System.out.println("use lambda");
            System.out.println("use lambda...");
        });

        //4. 参数：使用简化的lambda(只适用于只有一条语句的情况)
        test(() -> System.out.println("use simplified lambda"));
        
        //5. 如果Lambda要表达的函数方案已经存在于某个方法的实现中，
        //那么则可以通过双冒号来引用该方法作为Lambda的替代者（即方法引用）
        test(() -> System.out.println());
        test(System.out::println);
    }
}
```

## 函数式接口练习Demo02

```java
public class LambdaDemo {
    static String salutation = "Hello! ";

    @FunctionalInterface
    public interface MathOperation {
        int operation(int a, int b);
    }
    @FunctionalInterface
    private interface GreetingService {
        void sayMessage(String message);
    }
    @FunctionalInterface
    public interface Converter<T1, T2> {
        void convert(int i);
    }

    private int operate(int a, int b, MathOperation mathOperation){
        return mathOperation.operation(a, b);
    }

    public static void main(String[] args) {
        LambdaDemo lambdaDemo = new LambdaDemo();

        MathOperation addition = (int a, int b) -> a + b;

        MathOperation subtraction = (a, b) -> a - b;

        MathOperation multiplication = (int a, int b) -> { return a * b; };

        MathOperation division = (int a, int b) -> a / b;

        MathOperation test = (a,b) -> 5;

        // write some implements of functional interface above
        // then test them below...
        System.out.println("10 + 5 = " + lambdaDemo.operate(10, 5, addition));
        System.out.println("10 - 5 = " + lambdaDemo.operate(11, 5, subtraction));
        System.out.println("10 x 5 = " + lambdaDemo.operate(10, 6, multiplication));
        System.out.println("10 / 5 = " + lambdaDemo.operate(10, 5, division));

        System.out.println("test...");
        System.out.println(lambdaDemo.operate(1,1,test));
        System.out.println(test.operation(1, 1));

        GreetingService greetService1 = message ->
                System.out.println("Hello " + message);

        GreetingService greetService2 = (message) ->
                System.out.println("Hello " + message);

        greetService1.sayMessage("Run");
        greetService2.sayMessage("Google");

        GreetingService greetService3 = message ->
                System.out.println(salutation + message);
        greetService3.sayMessage("You");
    }
}
```

## 常用函数式接口

### java.util.function.Supplier<T> 接口

- 仅包含一个无参的方法：T get()。
- 用来获取一个泛型参数指定类型的对象数据。
- Supplier<T>接口被称之为生产型接口,指定接口的泛型是什么类型,那么接口中的get方法就会生产什么类型的数据

```java
public class Demo01Supplier {
    //定义一个方法,方法的参数传递Supplier<T>接口,泛型执行String,get方法就会返回一个String
    public static String getString(Supplier<String> sup){
        return sup.get();
    }
    public static void main(String[] args) {
        //调用getString方法,方法的参数Supplier是一个函数式接口,所以可以传递Lambda表达式
        String s = getString(()->{
            //生产一个字符串,并返回
            return "胡歌";
        });
        System.out.println(s);
        //优化Lambda表达式
        String s2 = getString(()->"胡歌");
        System.out.println(s2);
    }
}
```



```java
/*
    练习：求数组元素最大值
        使用Supplier接口作为方法参数类型，通过Lambda表达式求出int数组中的最大值。
        提示：接口的泛型请使用java.lang.Integer类。
 */
public class Demo02Test {
   //定义一个方法,用于获取int类型数组中元素的最大值,方法的参数传递Supplier接口,泛型使用Integer
   public static int getMax(Supplier<Integer> sup){
       return sup.get();
   }
    public static void main(String[] args) {
        //定义一个int类型的数组,并赋值
        int[] arr = {100,0,-50,880,99,33,-30};
        //调用getMax方法,方法的参数Supplier是一个函数式接口,所以可以传递Lambda表达式
        int maxValue = getMax(()->{
            //获取数组的最大值,并返回
            //定义一个变量,把数组中的第一个元素赋值给该变量,记录数组中元素的最大值
            int max = arr[0];
            //遍历数组,获取数组中的其他元素
            for (int i : arr) {
                //使用其他的元素和最大值比较
                if(i>max){
                    //如果i大于max,则替换max作为最大值
                    max = i;
                }
            }
            //返回最大值
            return max;
        });
        System.out.println("数组中元素的最大值是:"+maxValue);
    }
}
```

### java.util.function.Consumer<T>

- 该接口则正好与Supplier接口相反，它不是生产一个数据，而是消费一个数据，其数据类型由泛型决定。
- Consumer接口中包含抽象方法void accept(T t)，意为消费一个指定泛型的数据。
- Consumer接口是一个消费型接口,泛型执行什么类型,就可以使用accept方法消费什么类型的数据
- 至于具体怎么消费(使用),需要自定义

```java
public class Demo01Consumer {
    /*
        定义一个方法
        参数1传递一个字符串的姓名
        参数2传递Consumer接口,泛型使用String
        可以使用Consumer接口消费字符串的姓名
     */
    public static void method(String name, Consumer<String> con){
        con.accept(name);
    }
    public static void main(String[] args) {
        //调用method方法,传递字符串姓名,方法的另一个参数是Consumer接口,是一个函数式接口,所以可以传递Lambda表达式
        method("赵丽颖",(String name)->{
            //对传递的字符串进行消费
            //消费方式:直接输出字符串
            //System.out.println(name);
            //消费方式:把字符串进行反转输出
            String reName = new StringBuffer(name).reverse().toString();
            System.out.println(reName);
        });
    }
}
```

- Consumer接口的默认方法andThen
- 作用: 需要两个Consumer接口,可以把两个Consumer接口组合到一起,再对数据进行消费
- 例如:

```java
public static void main(String[] args) {
    Consumer<String> con1 = System.out::println;
    Consumer<String> con2 = s -> System.out.println(new StringBuilder(s).reverse());

    Consumer<String> consumer = con1.andThen(con2);
    consumer.accept("jack");
}
```



# Stream函数式编程

> [Java Stream流（详解）](https://blog.csdn.net/m0_60489526/article/details/119984236)

- Stream 是 Java8 中处理集合的关键抽象概念，它可以指定你希望对集合进行的操作，可以执行非常复杂的查找、过滤和映射数据等操作。使用Stream API 对集合数据进行操作，就类似于使用 SQL 执行的数据库查询。也可以使用 Stream API 来并行执行操作。简而言之，Stream API 提供了一种高效且易于使用的处理数据的方式
- 特点：
  - 不是数据结构，不会保存数据。	
  - 不会修改原来的数据源，它会将操作后的数据保存到另外一个对象中。（保留意见：毕竟peek方法可以修改流中元素）
  - 惰性求值，流在中间处理过程中，只是对操作进行了记录，并不会立即执行，需要等到执行终止操作的时候才会进行实际的计算。

## Stream类常用方法

| 方法名称 | 方法作用   | 方法种类 | 是否支持链式调用 |
| -------- | ---------- | -------- | ---------------- |
| count    | 统计个数   | 终结方法 | 否               |
| forEach  | 逐一处理   | 终结方法 | 否               |
| filter   | 过滤       | 函数拼接 | 是               |
| limit    | 取用前几个 | 函数拼接 | 是               |
| skip     | 跳过前几个 | 函数拼接 | 是               |
| map      | 映射       | 函数拼接 | 是               |
| concat   | 组合       | 函数拼接 | 是               |

## 特殊方法

| 方法名称 | 方法作用                 | 方法种类 | 是否支持链式调用 |
| -------- | ------------------------ | -------- | ---------------- |
| collect  | 把流中的数据收集到集合中 |          |                  |

## 示例Demo

```java
public class StreamDemo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("张老三");
        list.add("张小三");
        list.add("李四");
        list.add("赵五");
        list.add("张六");
        list.add("王八");

        Stream<String> stream1 = list.stream()
                .filter(s -> s.length() == 2);
        stream1.forEach(System.out::println);

        List<String> res = list.stream()
                .filter(s -> s.startsWith("张"))
                .filter(s -> s.length() == 3)
                .collect(Collectors.toList());

        res.forEach(System.out::println);
    }
}
```

## Map示例

### 从类中提取某个字段

`List idList=list.stream().map(Order::getId()).collect(Collectors.toList());`