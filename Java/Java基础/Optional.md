Optional 的好处在于可以简化平日里一系列判断 null 的操作，使得用起来的时候看着不需要判断 null，纵享丝滑，表现出来好像用 Optional 就不需要关心空指针的情况。

Optional 是为了清晰地表达返回值中没有结果的可能性

关于 POJO 里面的属性是否应该被 Optional 包裹，或者说是否应该把 get 方法包裹成 Optional 返回。我觉得 Optional 的用处就是逻辑处理的时候避免判空，仅此而已，所以 POJO 本该如何还是如何，Optional 应该交由逻辑处理代码来用

## Java Optional类

### 如何创建Optional

- 使用静态方法 ofNullable() 创建一个即可空又可非空的 Optional 对象

### 检查Optional是否有值

- 2种方法

```java
public boolean isPresent() {
    return value != null;
}
public void ifPresent(Consumer<? super T> consumer) {
    if (value != null)
        consumer.accept(value);
}
```

#### 常用方法

- map：转化值
- flatMap：同上，区别[参考](https://blog.csdn.net/qq_35634181/article/details/101109300)
- filter：过滤值
- [参考](https://blog.csdn.net/qing_gee/article/details/104767082)

### orElse() 和 orElseGet() 的不同之处

- 当user=null时，二者都会执行createNewUser，但当user!=null时，由于orElseGet的参数是Supplier函数式接口，createNewUser是不会执行的
- 在执行较密集的调用时，比如调用 Web 服务或数据查询，这个差异会对性能产生重大影响。

```java
User user = null
logger.debug("Using orElse");
User result = Optional.ofNullable(user).orElse(createNewUser());
logger.debug("Using orElseGet");
User result2 = Optional.ofNullable(user).orElseGet(() -> createNewUser());

private User createNewUser() {
    logger.debug("Creating New User");
    return new User("extra@gmail.com", "1234");
}
```

# 参考

- https://mp.weixin.qq.com/s/ElXyxOCJgIYhR9CcRs2c-g