## 前置知识

- 可以了解下常量池在Java6、7、8中的区别
- 看这个图就明白了

<img src="./resource/image-20220415180836452.png" alt="image-20220415180836452" style="zoom:50%;" />

## intern()方法简介

- 属于 native 方法
- 是一种`手动将字符串加入常量池`中的方法，intern()方法每次操作都需要与常量池中的数据进行比较，查看常量池中是否存在等值数据，所以其主要适用于有限值，并且这些有限值会被重复利用的场景，这样可以减少内存消耗

## Java6中的intern方法

- 调用 intern() 方法，尝试将向字符串常量池添加字符串
  - 如果字符串常量池中没有相等的字符串，则会将`字符串的内容`复制一份到池中，然后返回池中的字符串地址
  - 如果字符串常量池中存在相等的字符串，则返回池中的字符串地址

## Java7（及之后）中的intern方法

- 调用 intern() 方法，尝试将向字符串常量池添加字符串
  - 如果字符串常量池中没有相等的字符串，则会将`堆中字符串的地址`复制一份到池中，然后返回池中的字符串的地址。也就是说，此时 intern 方法返回的就是 Java 堆中字符串对象的地址
  - 如果字符串常量池中存在相等的字符串，则返回池中的字符串地址

## 本质区别

- 一个是复制值，一个是复制地址
- 所以下面这段代码，结果是不一样的：

```java
String str1 = new String("hello") + new String("world");
str1.intern();
String str2 = "helloworld";
System.out.println(str1 == str2); //Java6：false Java7：true
```



## String在编译期的操作

- 编译期能确定的String字面量将自动被放入字符串常量池中
- 那么哪些情况能编译器确定字面量？
  - 使用双引号括起来的`" "`的字符串
  - 字符串拼接时，只要不是`引用/变量`和字面量的拼接，最终都可以在编译期确定，比如：
    - 常量字符串的 "+" 操作，会拼接在一起放入常量池
    - final 字段修饰的，会进行常量替换，最终拼接在一起放入常量池

## 参考

- [String类的intern方法学习](https://blog.csdn.net/u014454538/article/details/121735718)

## 练习0

```java
String s1 = "abc";
String s2 = new String("abc");
String s3 = "abc";
System.out.println(s1 == s2);//false
System.out.println(s1.equals(s2));//true
System.out.println(s1 == s3);//true
```

- s1和s2的区别：
  - s1在常量池中有一个对象
  - s2在内存中有两个对象（堆new一个、常量池"abc"一个）
- string类覆写了object的`equals方法，该方法用于判读字符串的值`是否相同，而`==用于比较地址`是否相同
- s3发现该字符串已经在内存中了，就不会再开辟空间了，所以它也指向同一个对象

## 练习1：这段代码创建了几个对象？

```
String str3 = new String("a") + new String("a");
```

- 答案是五个。因为使用+号的String字符串拼接，底层其实都是先创建一个StringBuilder对象，然后调用append方法把要+的字符串都append进去，最后toString创建一个新的String对象str3，然后还有2个new的对象，常量池还有1个

## 练习2

```
String str1 = new String("hello, " + "world");
System.out.println(str1.intern() == str1); // 返回false，编译期，拼接出字符串字面量hello, world

String str3 = new String("test") + "Method";
System.out.println(str3.intern() == str3); // 返回true，编译期只能确定test和Method两个字符串字面量
```

## 练习3（环境：Java8）

```java
public static void main(String[] args) {
    String str1 = new String("hello") + " world";
    str1.intern();
    String str2 = "hello world";
    System.out.println("str1 == str2: " + (str1 == str2));//true，intern把str1的地址复制到了常量池，str2就用的常量池

    String str3 = new String("hello") + " lucy";
    String str4 = "hello lucy";
    str3.intern();
    System.out.println("str3 == str4: " + (str3 == str4));//false，intern看常量池已经有了，就没有操作
}
```

## 练习4：特殊字符串（冷知识）

```java
public static void main(String[] args) {
    String str1 = new String("open") + "jdk";
    String str2 = str1.intern();
    System.out.println("str1 == str2: " + (str1 == str2));  // JDK 1.8中实际返回false，因为openjdk比较特殊，因为sun.misc.Version 类中定义了内容为openjdk的静态字符串常量，当然还有其他的特殊字符串
}
```