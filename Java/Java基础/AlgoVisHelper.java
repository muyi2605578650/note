import java.awt.*;
import java.awt.geom.Ellipse2D;

public class AlgoVisHelper {

  private AlgoVisHelper() {
  }

  /**
   * 绘制一个空心圆
   *
   * @param g 绘图对象
   * @param x 圆心的 x 坐标
   * @param y 圆心的 y 坐标
   * @param r 圆的半径
   */
  public static void strokeCircle(Graphics2D g, int x, int y, int r) {
    Ellipse2D circle = new Ellipse2D.Double(x - r, y - r, 2 * r, 2 * r);
    g.draw(circle);
  }

  /**
   * 填充一个圆
   *
   * @param g 绘图对象
   * @param x 圆心的 x 坐标
   * @param y 圆心的 y 坐标
   * @param r 圆的半径
   */
  public static void fillCircle(Graphics2D g, int x, int y, int r) {
    Ellipse2D circle = new Ellipse2D.Double(x - r, y - r, 2 * r, 2 * r);
    g.fill(circle);
  }

  /**
   * 设置绘图对象的颜色
   *
   * @param g     绘图对象
   * @param color 颜色
   */
  public static void setColor(Graphics2D g, Color color) {
    g.setColor(color);
  }

  /**
   * 设置绘图对象的线条宽度
   *
   * @param g 绘图对象
   * @param w 线条宽度
   */
  public static void setStrokeWidth(Graphics2D g, int w) {
    int strokeWidth = w;
    g.setStroke(new BasicStroke(strokeWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
  }
}
