### 将double类型转化为BigDecimal时需要注意什么？

- double表示的小数是不精确的，如0.1这个数字，double只能表示他的近似值。
- 所以，当我们使用new BigDecimal(0.1)创建一个BigDecimal 的时候，其实创建出来的值并不是正好等于0.1的。
- 而是0.1000000000000000055511151231257827021181583404541015625。这是因为doule自身表示的只是一个近似值。

### 想要创建一个能精确的表示0.1的BigDecimal，请使用以下2种方式：

- BigDecimal recommend1 = new BigDecimal(\"0.1\");
- BigDecimal recommend2 = BigDecimal.valueOf(0.1);

### BigDecimal简介

- BigDecimal主要是通过一个无标度值和标度来表示的
- 如果scale为零或正值，则该值表示这个数字小数点右侧的位数。如果scale为负数，则该数字的真实值需要乘以10的该负数的绝对值的幂。例如，scale为-3，则这个数需要乘1000，即在末尾有3个0。
- BigDecimal中还提供了scale()方法，用来返回这个BigDecimal的标度
  - 如123.123，那么如果使用BigDecimal表示，那么他的无标度值为123123，他的标度为3。
  - 而二进制无法表示的0.1，使用BigDecimal就可以表示了，及通过无标度值1和标度1来表示。

### 为什么阿里巴巴禁止使用BigDecimal的equals方法做等值比较？

```java
BigDecimal bigDecimal = new BigDecimal(1);
BigDecimal bigDecimal1 = new BigDecimal(1);
System.out.println(bigDecimal.equals(bigDecimal1));

BigDecimal bigDecimal2 = new BigDecimal(1);
BigDecimal bigDecimal3 = new BigDecimal(1.0);
System.out.println(bigDecimal2.equals(bigDecimal3));

BigDecimal bigDecimal4 = new BigDecimal("1");
BigDecimal bigDecimal5 = new BigDecimal("1.0");
System.out.println(bigDecimal4.equals(bigDecimal5));
```

- 以上代码，输出结果为：true true false
- equals方法会比较两部分内容，分别是值（value）和标度（scale）

#### BigDecimal的标度问题

- 首先，BigDecimal一共有以下4个构造方法：
  - BigDecimal(int)
  - BigDecimal(double) 
  - BigDecimal(long) 
  - BigDecimal(String)

---

- 最简单的就是BigDecimal(long) 和BigDecimal(int)，因为是整数，所以标度就是0 
- 对于BigDecimal(double) ，当我们使用new BigDecimal(0.1)创建一个BigDecimal 的时候，其实创建出来的值并不是正好等于0.1的，而是0.1000000000000000055511151231257827021181583404541015625 。这是因为doule自身表示的只是一个近似值。
- 那么，无论我们使用new BigDecimal(0.1)还是new BigDecimal(0.10)定义，他的近似值都是0.1000000000000000055511151231257827021181583404541015625这个，那么他的标度就是这个数字的位数，即55。
  - 对于new BigDecimal(1.0)这样的形式来说，因为他本质上也是个整数，所以他创建出来的数字的标度就是0。
  - 所以，因为BigDecimal(1.0)和BigDecimal(1.00)的标度是一样的，所以在使用equals方法比较的时候，得到的结果就是true。
- 当我们使用new BigDecimal("0.1")创建一个BigDecimal 的时候，其实创建出来的值正好就是等于0.1的。那么他的标度也就是1。如果使用new BigDecimal("0.10000")，那么创建出来的数就是0.10000，标度也就是5。

---

- BigDecimal中提供了compareTo方法，这个方法就可以只比较两个数字的值，如果两个数相等，则返回0。

### 参考

- [告警：线上慎用 BigDecimal ，坑的差点被开了...](https://mp.weixin.qq.com/s/4rDCWs0td8PEawf3dZtCtQ)
- [为什么阿里巴巴禁止使用BigDecimal的equals方法做等值比较？](https://mp.weixin.qq.com/s/iiZW9xr1Xb2JIaRFnWLZUg)
