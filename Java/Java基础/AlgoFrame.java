import javax.swing.*;
import java.awt.*;

// AlgoFrame 类继承自 JFrame，用于构建一个包含算法可视化组件的窗口环境
public class AlgoFrame extends JFrame {

  // 定义画布的宽度和高度
  private final int canvasWidth;
  private final int canvasHeight;

  /**
   * 构造函数，初始化带有标题、画布宽度和画布高度的 AlgoFrame 实例。
   *
   * @param title        窗口的标题
   * @param canvasWidth  画布的宽度
   * @param canvasHeight 画布的高度
   */
  public AlgoFrame(String title, int canvasWidth, int canvasHeight) {

    super(title);

    this.canvasWidth = canvasWidth;
    this.canvasHeight = canvasHeight;

    // 创建并设置 AlgoCanvas 作为内容面板
    AlgoCanvas canvas = new AlgoCanvas();
    setContentPane(canvas);

    // 自动调整窗口大小以适应内容面板
    pack();

    // 设置窗口关闭时退出程序
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    // 禁止用户改变窗口大小
    setResizable(false);

    // 设置窗口可见
    setVisible(true);
  }

  /**
   * 带有默认画布尺寸的构造函数，只接受窗口标题作为参数。
   *
   * @param title 窗口的标题
   */
  public AlgoFrame(String title) {

    this(title, 1024, 768);
  }

  /**
   * 获取画布的宽度。
   *
   * @return 画布的宽度值
   */
  public int getCanvasWidth() {
    return canvasWidth;
  }

  /**
   * 获取画布的高度。
   *
   * @return 画布的高度值
   */
  public int getCanvasHeight() {
    return canvasHeight;
  }

  // 内部类 AlgoCanvas 继承自 JPanel，用于绘制算法图形
  private class AlgoCanvas extends JPanel {

    // AlgoCanvas 构造函数，启用双缓冲技术提高绘制效率
    public AlgoCanvas() {
      super(true);
    }

    // 重写 paintComponent 方法进行具体的图形绘制
    @Override
    public void paintComponent(Graphics g) {
      super.paintComponent(g);

      // 将 Graphics 转为 Graphics2D 对象，以便使用更丰富的绘图功能
      Graphics2D g2d = (Graphics2D) g;

      // 开启抗锯齿功能提升图形质量
      RenderingHints hints = new RenderingHints(
              RenderingHints.KEY_ANTIALIASING,
              RenderingHints.VALUE_ANTIALIAS_ON);
      g2d.addRenderingHints(hints);

      // 核心绘图逻辑：先填充一个蓝色圆形，再绘制一个红色边框圆形
      AlgoVisHelper.setColor(g2d, Color.BLUE);
      AlgoVisHelper.fillCircle(g2d, canvasWidth / 2, canvasHeight / 2, 200);

      AlgoVisHelper.setStrokeWidth(g2d, 5);
      AlgoVisHelper.setColor(g2d, Color.RED);
      AlgoVisHelper.strokeCircle(g2d, canvasWidth / 2, canvasHeight / 2, 200);
    }

    // 重写 getPreferredSize 方法返回自定义的组件尺寸（与画布尺寸一致）
    @Override
    public Dimension getPreferredSize() {
      return new Dimension(canvasWidth, canvasHeight);
    }

  }
}
