# spring循环依赖

- 2个类都采用如下方式依赖注入会导致循环依赖

```java
public IdeaService(EsIdeaService esIdeaService) {
  this.esIdeaService = esIdeaService;
}
```

# 单例注入可以避免循环依赖

- 比如用@Autowired注入、setter注入
- spring是如何做到的？
- spring内部有三级缓存：
  - singletonObjects 一级缓存，用于保存实例化、注入、初始化完成的 bean 实例，即单例池  
  - earlySingletonObjects 二级缓存，用于保存实例化但属性还未填充完成的bean
  - singletonFactories 三级缓存，用于保存 bean 创建工厂，以便于后面扩展有机会创建代理对象。

```java
private final Map<String, Object> singletonObjects = new ConcurrentHashMap(256);
private final Map<String, ObjectFactory<?>> singletonFactories = new HashMap(16);
private final Map<String, Object> earlySingletonObjects = new ConcurrentHashMap(16);
```

![image-20240215214300352](./resource/image-20240215214300352.png)



![img](./resource/c7c341ba61f445959ca1e111ebdacd66.jpeg#pic_center)

#  A / B 两对象在三级缓存中的迁移说明

1. A 创建过程中需要 B，于是 A 将自己放到三级缓里面，去实例化 B。
2. B 实例化的时候发现需要 A，于是 B 先查一级缓存，没有，再查二级缓存，还是没有，再查三级缓存，找到了 A 然后把三级缓存里面的这个 A 放到二级缓存里面，并删除三级缓存里面的 A。
3. B 顺利初始化完毕，将自己放到一级缓存里面（此时 B 里面的 A 依然是创建中状态)，然后回来接着创建 A，此时 B 已经创建结束，直接从一级缓存里面拿到 B，然后完成创建，并将 A 自己放到一级缓存里面。

## 详细步骤

1. 调用 doGetBean() 方法，想要获取 beanA，于是调用 getSingleton() 方法从缓存中查找 beanA
2. 在 getSingleton() 方法中，从一级缓存中查找，没有，返回 null
3. doGetBean() 方法中获取到的 beanA 为 null，于是走对应的处理逻辑，调用 getSingleton() 的重载方法（参数为 ObjectFactory 的)
4. 在 getSingleton() 方法中，先将 beanA_name 添加到一个集合中，用于标记该 bean 正在创建中。然后回调匿名内部类的 creatBean 方法
5. 进入 AbstractAutowireCapableBeanFactory#ndoCreateBean，先反射调用构造器创建出 beanA 的实例，然后判断: 是否为单例、是否允许提前暴露引用 (对于单例一般为 true)、是否正在创建中（即是否在第四步的集合中）。判断为 true 则将 beanA 添加到【三级缓存】中
6. 对 beanA 进行属性填充，此时检测到 beanA 依赖于 beanB，于是开始查找 beanB
7. 调用 doGetBean() 方法，和上面 beanA 的过程一样，到缓存中查找 beanB，没有则创建，然后给 beanB 填充属性
8. 此时 beanB 依赖于 beanA，调用 getSingleton() 获取 beanA，依次从一级、二级、三级缓存中找，此时从三级缓存中获取到 beanA 的创建工厂，通过创建工厂获取到 singletonObject，此时这个 singletonObject 指向的就是上面在 doCreateBean() 方法中实例化的 beanA
9. 这样 beanB 就获取到了 beanA 的依赖，于是 beanB 顺利完成实例化，并将 beanA 从三级缓存移动到二级缓存中
10. 随后 beanA 继续他的属性填充工作，此时也获取到了 beanB，beanA 也随之完成了创建，回到 getsingleton() 方法中继续向下执行，将 beanA 从二级缓存移动到一级缓存中

# 核心代码

- 位于DefaultSingletonBeanRegistry类

```Java
@Nullable
protected Object getSingleton(String beanName, boolean allowEarlyReference) {
    Object singletonObject = this.singletonObjects.get(beanName);
    if (singletonObject == null && this.isSingletonCurrentlyInCreation(beanName)) {
        singletonObject = this.earlySingletonObjects.get(beanName);
        if (singletonObject == null && allowEarlyReference) {
            synchronized(this.singletonObjects) {
                singletonObject = this.singletonObjects.get(beanName);
                if (singletonObject == null) {
                    singletonObject = this.earlySingletonObjects.get(beanName);
                    if (singletonObject == null) {
                        ObjectFactory<?> singletonFactory = (ObjectFactory)this.singletonFactories.get(beanName);
                        if (singletonFactory != null) {
                            singletonObject = singletonFactory.getObject();
                            this.earlySingletonObjects.put(beanName, singletonObject);
                            this.singletonFactories.remove(beanName);
                        }
                    }
                }
            }
        }
    }

    return singletonObject;
}

protected void addSingleton(String beanName, Object singletonObject) {
  synchronized(this.singletonObjects) {
    this.singletonObjects.put(beanName, singletonObject);
    this.singletonFactories.remove(beanName);
    this.earlySingletonObjects.remove(beanName);
    this.registeredSingletons.add(beanName);
  }
}

protected void addSingletonFactory(String beanName, ObjectFactory<?> singletonFactory) {
  Assert.notNull(singletonFactory, "Singleton factory must not be null");
  synchronized(this.singletonObjects) {
    if (!this.singletonObjects.containsKey(beanName)) {
      this.singletonFactories.put(beanName, singletonFactory);
      this.earlySingletonObjects.remove(beanName);
      this.registeredSingletons.add(beanName);
    }

  }
}
```

# 为什么要用3级缓存

# 参考

https://blog.csdn.net/lkforce/article/details/97183065
