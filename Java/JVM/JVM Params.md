[toc]

## JVM 的参数类型

### 标配参数

- -version
- -help

### X 参数（了解）

- -Xint：解释执行
- -Xcomp：第一次使用就编译成本地代码
- -Xmixed：混合模式

### XX 参数

- Boolean 类型：-XX:+或者- 属性值（+表示开启，-表示关闭）
  - -XX:+PrintGCDetails：打印GC收集细节
  - -XX:-PrintGCDetails：不打印 
  - -XX:+UseSerialGC：使用串行收集器
  - -XX:-UseSerialGC：不使用
- KV 设置类型：-XX:key=value
  - XX:MetaspaceSize=128m 元空间大小
  - -XX:`MaxTenuringThreshold`=15 晋升老年代的年龄阈值
#### jinfo 命令 

- 作用：查看当前运行程序的配置
- 查看某个参数的值： `jinfo -flag [参数] [进程号]` 
  -  `jinfo -flag PrintGCDetails [进程号]` ：查看是否配置了 PrintGCDetails  （是否打印GC收集细节）
  - 结果： -XX:-PrintGCDetails
  - 可以看出默认是不打印 GC 收集细节

- 查看所有参数：`jinfo -flags [进程号]`

#### IDEA如何配置JVM参数

![](./resource/idea_jvm.png)

#### 查看 JVM 默认值

- 查看初始默认值：Java -XX:+PrintFlagsInitial

  ```shell
  [Global flags]
  ccstrlist AOTLibrary                               =                                           {product} {default}
        int ActiveProcessorCount                     = -1                                        {product} {default}
      uintx AdaptiveSizeDecrementScaleFactor         = 4                                         {product} {default}
      uintx AdaptiveSizeMajorGCDecayTimeScale        = 10                                        {product} {default}
      uintx AdaptiveSizePolicyCollectionCostMargin   = 50                                        {product} {default}
      uintx AdaptiveSizePolicyInitializingSteps      = 20                                        {product} {default}
  ```

  

- 查看修改更新：Java -XX:+PrintFlagsFinal

- = 与 := 的区别是，一个是默认，一个是人为改变或者 jvm 加载时改变的参数

- Java -XX:+PrintFlagsFinal -XX:MetaspaceSize=512m helloworld：运行helloworld程序，同时修改元空间大小，同时查看修改更新值

- 打印命令行参数(可以看**默认垃圾回收器**)：Java -XX:+**PrintCommandLineFlags**

  ```
  cuzz@cuzz-pc:~/Project/demo$ java -XX:+PrintCommandLineFlags-XX:InitialHeapSize=128789376 -XX:MaxHeapSize=2060630016 -XX:+PrintCommandLineFlags -XX:+UseCompressedClassPointers -XX:+UseCompressedOops -XX:+UseParallelGC 
  ```
  
  

## JVM 常用的基本配置参数有哪些？

### -Xms

- 初始堆内存，默认本机内存的1/64
- 等价于 -XX:InitialHeapSize
- 如 -Xms1024m

### -Xmx

- 最大堆内存，默认1/4
- 等价于 -XX:MaxHeapSize

### -Xss

- 设置单个线程栈的大小，一般默认为 512-1024k
- 等价于 -XX:ThreadStackSize
- 需要注意的是，`如果某个参数=0，表示使用默认配置，而不是0`

### -Xmn

- 设置年轻代的大小
- **整个JVM内存大小=年轻代大小 + 年老代大小 + 持久代大小**，持久代一般固定大小为64m，所以增大年轻代后，将会减小年老代大小。此值对系统性能影响较大，Sun官方推荐配置为整个堆的3/8。

### -XX:MetaspaceSize

- 设置元空间大小（元空间（Java8才有）的本质和永久代类似，都是对 JVM 规范中的方法区的实现，不过元空间于永久代之间最大区别在于，`元空间并不在虚拟中，而是使用本地内存`，因此默认情况下，元空间的大小仅受本地内存限制）
- 元空间默认（21MB）比较小，我们可以调大一点（比如256MB）

### -XX:+PrintGCDetails

- 用于查看详细的 GC 日志信息
- 设置 JVM 参数为： -Xms10m -Xmx10m -XX:+PrintGCDetails，然后故意使用较大的内存，就可以看到OOM信息以及GC日志信息

常用配置：

![image-20211017165636073](./resource/image-20211017165636073.png)

控制台部分GC日志：

![image-20211017170004514](./resource/image-20211017170004514.png)

YGC日志信息解读：

![image-20211017165858359](./resource/image-20211017165858359.png)

FGC日志信息解读：

![image-20211017170150051](./resource/image-20211017170150051.png)

### -XX:SurvivorRatio

- 设置新生代中 eden 和 S0/S1 空间比例
- 默认 -XX:SurvivorRatio=8，Eden : S0 : S1 = 8 : 1 : 1

### -XX:NewRatio

- 配置新生代和老年代在堆中的占比
- 默认 -XX:NewRatio=2 新生代占1，老年代占2，新生代占整个堆的 1/3
- -XX:NewRatio=4 新生代占1，老年代占4

### -XX:MaxTenuringThreshold

- 设置垃圾最大年龄，默认15，最大也是15

### -XX:MaxDirectMemorySize

- 最大直接内存，即本地内存，一般默认是本机物理内存的1/4，其实就是最大堆内存

## 常见的OOM错误 

### java.lang.StackOverflowError

- 产生原因：在一个函数中调用自己就会产生这个错误

### java.lang.OutOfMemoryError : Java heap space

- 产生原因：new 一个很大对象

### java.lang.OutOfMemoryError : GC overhead limit exceeded

- 产生原因：执行垃圾收集的时间比例太大， 有效的运算量太小，默认情况下,，如果GC花费的时间超过 **98%**， 并且GC回收的内存少于 **2%**， JVM就会抛出这个错误。

### java.lang.OutOfMemoryError : Direct buffer memory

- 产生原因：直接/堆外内存满了，和GC无关（GC不会管堆外内存），可能是直接分配堆外内存的程序导致的

### java.lang.OutOfMemoryError : unable to create new native thread

- 这个问题经常问，因为和高并发、多线程有关
- 产生原因：一个进程创建线程太多了，Linux系统默认单个进程创建的最大线程数是1024个

![image-20220130104522429](./resource/image-20220130104522429.png)

### java.lang.OutOfMemoryError : Metaspace

- 产生原因：元空间满了，元空间用的本地内存
