## 在JPA中使用in原生SQL

```java
@Query(value="select * from tbl_name where name in (:names) ",nativeQuery = true)
List<Bean> findAllByName(@Param("name") List<String> names);
```

## JPA封装自定义查询结果

有几种方法：

1. 定义接口（只能取值，可能还有其他问题，比如不能转成json）
2. 定义类
3. 使用Object[]数组（数据少可以用这个）

参考：https://blog.csdn.net/fanglinxing/article/details/116991614