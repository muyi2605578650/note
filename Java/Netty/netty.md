参考  https://www.bilibili.com/video/BV1my4y1L71f/?spm_id_from=333.999.0.0&vd_source=31dcc6154387b50eb9c06221a8965dd5

# Java  NIO实现

```Java
/**
 * NIOServer类实现了基于非阻塞I/O的服务器端程序。
 * 通过ServerSocketChannel和Selector来监听客户端连接和读取数据。
 */
package com.space.java.net;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;

public class NIOServer {
  /**
   * 程序入口，启动服务器并开始监听端口9000。
   * @param args 命令行参数（未使用）
   * @throws IOException 如果发生I/O错误
   */
  public static void main(String[] args) throws IOException {
    // 打开服务器套接字通道并绑定到端口9000
    final ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
    serverSocketChannel.socket().bind(new InetSocketAddress(9000));
    serverSocketChannel.configureBlocking(false); // 配置为非阻塞模式

    // 打开选择器并注册服务器套接字通道，监听接受操作
    final Selector selector = Selector.open();
    final SelectionKey selectionKey = serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

    System.out.println("service start......");

    // 不断轮询选择器，处理连接和读取操作
    while (true) {
      selector.select(); // 阻塞，等待就绪的操作
      final Set<SelectionKey> selectionKeys = selector.selectedKeys();
      final Iterator<SelectionKey> iterator = selectionKeys.iterator();

      // 遍历并处理就绪的操作
      while (iterator.hasNext()) {
        final SelectionKey key = iterator.next();
        if (key.isAcceptable()) { // 处理连接请求
          final ServerSocketChannel channel = (ServerSocketChannel) key.channel();
          final SocketChannel socketChannel = channel.accept(); // 接受连接
          socketChannel.configureBlocking(false); // 配置为非阻塞模式
          final SelectionKey selKey = socketChannel.register(selector, SelectionKey.OP_READ); // 注册读操作
          System.out.println("client connect success......");
        } else if (key.isReadable()) { // 处理读取操作（代码未实现）
          final SocketChannel socketChannel = (SocketChannel) key.channel();
          final ByteBuffer buffer = ByteBuffer.allocate(128);
          final int len = socketChannel.read(buffer);
          if (len > 0) {
            System.out.println("receive msg: " + new String(buffer.array()));
          } else if (len == -1) {
            System.out.println("client clost connection");
            socketChannel.close();
          }
        }
        iterator.remove();
      }
    }
  }
}
```

## Selector（多路复用器）

1.  `Java` 的 `NIO`，用非阻塞的 `IO` 方式。可以用一个线程，处理多个的客户端连接，就会使用到 `Selector`，Selector底层在Linux的实现就是epoll
2.  `Selector` 能够检测多个注册的通道上是否有事件发生（注意：多个 `Channel` 以事件的方式可以注册到同一个 `Selector`），如果有事件发生，便获取事件然后针对每个事件进行相应的处理。这样就可以只用一个单线程去管理多个通道，也就是管理多个连接和请求。
3.  只有在连接 / 通道真正有读写事件发生时，才会进行读写，就大大地减少了系统开销，并且不必为每个连接都创建一个线程，不用去维护多个线程。
4.  避免了多线程之间的上下文切换导致的开销。

# Netty

netty其实就是封装了Java nio，然后做了进一步优化

![image-20240318221046986](./resource/image-20240318221046986.png)

`Netty` 的 `IO` 线程 `NioEventLoop` 聚合了 `Selector`（选择器，也叫多路复用器），可以同时并发处理成百上千个客户端连接。