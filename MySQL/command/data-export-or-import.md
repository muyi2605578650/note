## load data infile 命令

```mysql
LOAD DATA LOCAL INFILE 'C:/Users/sdfads/Desktop/fsdf.txt' 
INTO TABLE user_tag 
FIELDS TERMINATED BY ',' 
LINES TERMINATED BY '\r\n' 
IGNORE 1 LINES ( user_id, tg_id)
set user_id = NULL, user_tag = 2, create_time='2022-11-14 13:30:00', update_time='2022-11-14 12:30:00';
```

- 该文件中的内容就是user_id, tg_id这两个字段
- 参考：https://fullstack.blog.csdn.net/article/details/112377942