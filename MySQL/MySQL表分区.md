# MySQL表分区

## 表分区的概念

- 分区和分表不是一回事，**分区是将同一表中不同行的记录分配到不同的物理文件中，几个分区就有几个.idb文件**
- MySQL在5.1时添加了对水平分区的支持
- 通过查看`have_partition_engine`的值来确定你的MySQL版本是否支持分区
- 表分区后，如果sql语句不指定分区，则会走所有分区
- MySQL是面向`OLTP`的数据库，而分区一般用在`OLAP`类型的数据库，所以对于分区的使用应该小心，如果不清楚如何使用分区可能会对性能产生负面的影响。

## 分区类型

目前MySQL支持以下几种类型的分区：

1. RANGE分区：基于一个给定区间边界，得到若干个连续区间范围，按照分区键的落点，把数据分配到不同的分区；（`实战中，十有八九都是用RANGE分区`）
2. LIST分区：类似RANGE分区，区别在于LIST分区是基于枚举出的值列表分区，RANGE是基于给定连续区间范围分区；
3. HASH分区：基于用户自定义的表达式的返回值，对其根据分区数来取模，从而进行记录在分区间的分配的模式。这个用户自定义的表达式，就是MySQL希望用户填入的哈希函数。
4. KEY分区：类似于按HASH分区，区别在于KEY分区只支持计算一列或多列，且使用MySQL 服务器提供的自身的哈希函数。

如果表存在主键或者唯一索引时，分区列必须是唯一索引的一个组成部分。

## Range分区实战

```MySQL
CREATE TABLE
    `Order` (
        `id`
        INT NOT NULL AUTO_INCREMENT,
        `partition_key`
        INT NOT NULL,
        `amt`
        DECIMAL(5) NULL) PARTITION BY RANGE(partition_key)
PARTITIONS 5(
    PARTITION part0 VALUES LESS THAN(201901),
    PARTITION part1 VALUES LESS THAN(201902),
    PARTITION part2 VALUES LESS THAN(201903),
    PARTITION part3 VALUES LESS THAN(201904),
    PARTITION part4 VALUES LESS THAN(201905),
    PARTITION part4 VALUES LESS THAN MAXVALUE;
```



## 参考

- [参考](https://zhuanlan.zhihu.com/p/342814592)
