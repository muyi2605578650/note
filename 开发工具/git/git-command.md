## 如何回退远程仓库

假设现在远程仓库和本地是一致的

1. 先本地回退：git reset --hard commitID
2. 强制覆盖到远程仓库：git push origin HEAD --force

参考：https://www.cnblogs.com/ZhangRuoXu/p/6706552.html

## 把某个分支的某个commit合并到当前分支

我唯一用到这个是因为有个commit提交到了错误的分支，需要只将这个commit合并到正确的分支

```
git branch
# 当前分支为 aaa
git log 
# 此时会出现aaa分支的提交记录，假设此时你想要将aaa分支的commitId为afew43f3as3r32dfs的提交合并到bbb分支
git checkout bbb
git cherry-pick afew43f3as3r32dfs
# 此时便已经将这个commit合并到bbb分支了
git log
# 通过git log 便可以看到
```

参考：https://arrow.blog.csdn.net/article/details/107067991