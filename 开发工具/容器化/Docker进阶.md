[toc]

# Docker容器数据卷

- 是指容器间数据共享，以及容器和宿主机数据共享技术
- 使用命令：docker run -it -v /home/ceshi:/home centos /bin/bash
- 如果容器被删除，那么宿主机挂载的卷还在吗？在（容器数据持久化）

## 使用MySQL测试共享数据

- docker run -d -p 3306:3306 -v /home/mysql/conf:/etc/mysql/conf.d -v /home/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 --name mysql01 mysql/mysql-server
- 其中 -e 是指环境配置

## Docker volume命令

- 用于查看docker的所有数据卷
- docker volume ls：查看所有的volume的情况
- docker volumn inspect 卷名：查看某个卷的信息（路径等）
- 所有的 docker 容器内的卷，没有指定目录的情况下都是在/var/lib/docker/volumes/xxxx/下
- 如果指定了目录，docker volume ls 是查看不到的

## 具名和匿名挂载

- 匿名挂载：-v 容器内路径
- 具名挂载：-v 卷名:容器内路径
- 指定路径挂载 docker volume ls 是查看不到的：-v /宿主机路径:容器内路径

## 设置容器卷ro、rw

- ro ：readonly 只读
- rw ：readwrite 可读可写（默认）
- docker run -d -P --name nginx05 -v juming:/etc/nginx:ro nginx
- docker run -d -P --name nginx05 -v juming:/etc/nginx:rw nginx
- ro表示这个路径只能通过宿主机来操作，容器内部无法操作！

# DockerFile

- 是用来构建 docker 镜像的文件
- Dockerfile 是面向开发的，要发布项目，做镜像，就需要编写 dockerfile 文件
- 特点：
    - 默认命名是Dockerfile
    - 每个关键字 (指令）都是必须是大写字母
    - 执行从上到下顺序
    - ''#'' 表示注释
    - 每一个指令都会创建提交一个新的镜像层

```dockerfile
FROM centos
VOLUME ["/volume01","/volume02"] # 容器内挂载出来的目录
CMD echo "----end----"
CMD /bin/bash
# 这里的每个命令，就是镜像的一层！
```
## DockerFile常用指令

```shell
FROM				# 基础镜像，一切从这里开始构建
MAINTAINER			# 镜像是谁写的， 姓名+邮箱
RUN					# 镜像构建的时候需要运行的命令
ADD					# 添加内容到容器内指定目录
WORKDIR				# 镜像的工作目录
VOLUME				# 挂载的目录
EXPOSE				# 声明需要暴露的端口，只是声明，run的时候需要-p指定duan
CMD					# 指定这个容器启动的时候要运行的命令，只有最后一个会生效，可被替代。
ENTRYPOINT			# 指定这个容器启动的时候要运行的命令
ONBUILD				# 当构建一个被继承 DockerFile 这个时候就会运行ONBUILD的指令，触发指令。
COPY				# 类似ADD，将我们文件拷贝到镜像中
ENV					# 构建的时候设置环境变量
```

- 注意CMD和ENTRYPPOINT的区别

## 练习1：使用DockerFile构建自定义的centos镜像

- 编写dockerfile：

```dockerfile
FROM centos
MAINTAINER name<123@qq.com>

ENV MYPATH /usr/local # 环境变量
WORKDIR $MYPATH # 工作目录，即进入容器后的目录

RUN yum -y install vim # 构建镜像的时候执行一些命令
RUN yum -y install net-tools

EXPOSE 80 # 暴露端口

CMD echo $MYPATH
CMD echo "-----end----"
CMD /bin/bash # 指定shell
```

- 构建镜像，使用命令：`docker build -f ./dockerfile -t image_name:tag .`（注意最后有个点，用于指定镜像构建过程中的上下文环境的目录）
- 查看镜像的构建历史：docker history image_id

## 练习2：自制tomcat镜像并发布项目

```dockerfile
FROM centos 
MAINTAINER xxx<123@qq.com>
COPY README /usr/local/README #复制文件
ADD jdk-8u231-linux-x64.tar.gz /usr/local/ #复制解压
ADD apache-tomcat-9.0.35.tar.gz /usr/local/ #复制解压
RUN yum -y install vim
ENV MYPATH /usr/local #设置环境变量
WORKDIR $MYPATH #设置工作目录
ENV JAVA_HOME /usr/local/jdk1.8.0_231 #设置环境变量
ENV CATALINA_HOME /usr/local/apache-tomcat-9.0.35 #设置环境变量
ENV PATH $PATH:$JAVA_HOME/bin:$CATALINA_HOME/lib #设置环境变量 分隔符是：
EXPOSE 8080 #设置暴露的端口
CMD /usr/local/apache-tomcat-9.0.35/bin/startup.sh && tail -F /usr/local/apache-tomcat-9.0.35/logs/catalina.out # 设置默认命令
```

- 构建镜像：docker build -t mytomcat .
- 不使用-f 指定文件是因为：dockerfile命名使用默认命名
- 运行镜像：docker run -d -p 8080:8080 --name tomcat01 -v /home/kuangshen/build/tomcat/test:/usr/local/apache-tomcat-9.0.35/webapps/test -v /home/kuangshen/build/tomcat/tomcatlogs/:/usr/local/apache-tomcat-9.0.35/logs mytomcat:0.1
- 发布项目：由于做了卷挂载，直接在本地编写项目就可以发布（具体就不写了）

# 数据卷容器

- 可以理解为一个容器充当父数据卷容器，其他容器都可以共享该父容器的卷
- 使用命令：docker run -it --name docker02 `--volumes-from` docker01 image_name:tag （这样docker02就同步了docker01的卷）
- 同理可以创建docker03容器
- 删除docker01容器后，docker02、03还有共享的卷吗？有，所以可以理解为所有容器都保存了一个引用，该引用指向的是宿主机

## 拓展：多个 mysql 实现数据共享

- docker run -d -p 3306:3306 -v /home/mysql/conf:/etc/mysql/conf.d -v /home/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 --name mysql01 mysql:5.7 
- docker run -d -p 3307:3306 -e MYSQL_ROOT_PASSWORD=123456 --name mysql02 --**volumes-from** mysql01  mysql:5.7
- 这个时候，可以实现两个容器数据同步！

# 发布镜像

- 首先在docker官网注册账号
- 登录：docker login -u -p
- 先给镜像起一个标准名称：docker tag image_id ngc604/nginx03:1.0 （格式最好按照这样，ngc604是docker_id）
- 发布镜像：docker push ngc604/nginx03:1.0
- push成功后，使用docker search ngc604搜索成功就ok了
- 登出：docker logout

# Docker网络

- Docker网络有4种模式
  - bridge ：桥接 docker（默认，自己创建也是用bridge模式）。
  - none ：不配置网络，安全性高。使用场景较少。
  - host ：和宿主机共享网络。
  - container ：容器网络连通（用得少！局限很大）。
- [Docker四种网络模式](https://www.jianshu.com/p/22a7032bb7bd)，安装Docker后默认是`bridge`模式
- Docker网络中的容器和容器之间，容器和宿主机之间是否相通？都是互相连通的。Docker进程启动时，会在主机上创建一个名为docker0的虚拟网桥，所有Docker容器都会连接到这个虚拟网桥上。虚拟网桥的工作方式和二层交换机类似，这样主机上的所有容器就通过交换机连在了一个二层网络中。
- 换言之，Docker使用的是Linux的桥接，宿主机是一个Docker容器的网桥
- 当创建一个容器时，Docker会从docker0子网中分配一个IP给容器，并设置docker0的IP地址为容器的默认网关。Docker会在主机上创建一对虚拟网卡`veth pair设备`，veth pair设备的一端放在新创建的容器中，并命名为eth0（容器的网卡），另一端放在主机中，以vethxxx这样类似的名字命名，并将这个网络设备加入到docker0网桥中。
- veth-pair 就是一对的虚拟设备接口，他们都是成对出现的。OpenStack，Docker容器之间的连接，OVS的连接，都是使用veth-pair技术

<img src="./resource/image-20220120174956437.png" alt="image-20220120174956437" style="zoom: 50%;" />

## docker network 命令

- 使用docker network 可以查看docker网络信息

## --link命令（不建议使用）

- 用来链接2个容器，使得容器之间可以互相通信
- –link主要用来解决两个容器通过ip地址连接时`容器ip地址会变`的问题
- docker run -d -P --name tomcat03 --link tomcat02 tomcat（使得tomcat03可以通过ping tomcat02的方式而不用ip连接，但反之不行）
- **本质就是在容器的/etc/hosts配置中添加映射**
- --link已经不建议使用，可以使用自定义网络来解决ip地址变化的问题

##  自定义网络

- 在自定义的网络下，容器之间直接使用名称就可以ping通，不需要使用–link，所以最好自定义一个网络，不同的集群可以使用不同的子网，保证集群是安全和健康的
- 创建自定义网络：`docker network create --driver bridge --subnet 192.168.0.0/16 --gateway 192.168.0.1 mynet`
  - --driver：指定网络模式（不指定默认是bridge模式）
  - --subnet：指定子网
  - --gateway：指定网关ip
- 查看网络：docker network inspect mynet
- 在自定义网络中创建容器：docker run -d -P --name tomcat01 `--net mynet` tomcat
  - 当不指定--net时，等价于--net bridge，即默认的桥接网络名称
- 但docker中不同子网的容器无法直接通信，如果一个容器需要和其他子网的容器通信，需要指定命令：`docker network connect mynet tomcat02(容器名称)`，这样tomcat02就可以和mynet中所有容器通信了，相当于tomcat02有了双网卡

## 在自定义网络中搭建Redis集群

- 38节，待重看

# 练习：SpringBoot项目打包Docker镜像

- idea可以先下载一个插件，直接搜docker，该插件可以远程连接docker仓库，还可以高亮显示Dockerfile
- 打包项目，得到jar包
- 编写Dockerfile

```dockerfile
FROM openjdk
COPY *.jar /app.jar
CMD ["--server.port=8080"]
EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar"]
```

- 将jar包和Dockerfile放到服务器同一目录下
- docker build -t myspringbootpro .
- docker run -it -p 8080:8080 --name testpro myspringbootpro  /bin/bash
  - 或者-P，会随机指定宿主机端口映射到容器内8080
- 测试访问接口
