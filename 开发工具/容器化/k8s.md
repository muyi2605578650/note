## 什么是 [Kubernetes](https://kubernetes.io/)

- [Kubernetes](https://kubernetes.io/) 是一个开源的容器编排引擎，可以用来管理容器化的应用，包括容器的自动化的部署、扩容、缩容、升级、回滚等等，它是 Google 在 2014 年开源的一个项目，它的前身是 Google 内部的 Borg 系统。

## 为什么要使用 [Kubernetes](https://kubernetes.io/)

- 在 [Kubernetes](https://kubernetes.io/) 出现之前，我们一般都是使用 Docker 来管理容器化的应用，但是 Docker 只是一个单机的容器管理工具，它只能管理单个节点上的容器，当我们的应用程序需要运行在多个节点上的时候，就需要使用一些其他的工具来管理这些节点，比如 Docker Swarm、Mesos、[Kubernetes](https://kubernetes.io/) 等等，这些工具都是容器编排引擎，它们可以用来管理多个节点上的容器，但是它们之间也有一些区别，比如 Docker Swarm 是Docker 官方提供的一个容器编排引擎，它的功能比较简单，适合于一些小型的、简单的场景，而 Mesos 和 [Kubernetes](https://kubernetes.io/) 则是比较复杂的容器编排引擎，Mesos 是 Apache基金会的一个开源项目，而 [Kubernetes](https://kubernetes.io/) 是 Google 在 2014 年开源的，目前已经成为了 CNCF（Cloud Native Computing Foundation）的一个顶级项目，基本上已经成为了容器编排引擎的事实标准了。

## K8S基本架构

- master
  - api-server：处理用户请求，向node发指令
  - ETCD数据库：存储各个节点状态等
  - controller-manager：各种资源自动化的控制中心
  - scheduler：调度
- 多个node
  - kubelet：和api-server通信
  - kube-proxy：虚拟网卡
  - 容器 pod：调度的最小单位
    - docker+pause（也是容器）


## 重要概念

- deployment：用于自动维持pod数量
- service：将deployment中多个pod抽象为一个服务（ip）对外暴露（负载均衡），service之间通信可以通过名称访问（k8s有dns）（service是k8s内的服务，其ip仍然不能被宿主机或外界访问）
- ingress：http映射，通过配置不同的域名可以访问k8s内的不同service。

## 命令

- kubectl get pod
- kubectl get deployments
- kubectl get service/svc
- kubectl edit deployments d1
- kubectl apply -f ing-dep.yml   生效ingress配置文件

## 参考

https://www.bilibili.com/video/BV18t411u7kX/?spm_id_from=333.999.0.0

https://www.bilibili.com/video/BV1Se411r7vY/?spm_id_from=333.999.0.0&vd_source=31dcc6154387b50eb9c06221a8965dd5