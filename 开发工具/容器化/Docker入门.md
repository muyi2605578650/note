[toc]

# Docker是什么

- docker是一种虚拟化技术，不同于虚拟机会模拟一个完整的OS，一个容器里只含有自己需要的运行环境，容器之间互相隔离，但它们用的还是同一个OS

![image-20211214123955609](./resource/1.png)

# apt-get安装Docker

- 参考：https://www.jianshu.com/p/03a3401eb214

# Yum安装Docker

- RedHat/CentOS必须要6.6版本以上，或者7.x才能安装docker

```bash
# 1. 卸载旧的版本
yum remove docker \
    docker-client \
    docker-client-latest \
    docker-common \
    docker-latest \
    docker-latest-logrotate \
    docker-engine
# 2. 安装依赖包
yum install -y yum-untils
# 3. 设置镜像仓库
yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo # 默认国外的镜像仓库
yum-config-manager \
    --add-repo \
    https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo # 阿里云的镜像仓库(推荐)
# 更新yum软件包索引
yum makecache fast
# 4. 安装Docker相关, docker-ce 社区版  docker-ee 企业版
yum install docker-ce docker-ce-cli containerd.io
# 安装指定版本
yum install docker-ce-<VERSION-STRING> docker-ce-cli-<VERSION-STRING> container.io
# 5. 启动Docker
systemctl start docker
# 5.1 设置docker开机启动
systemctl enable docker
# 6. 使用docker version查看是否安装成功
docker version
# 7. 测试 HelloWorld!
docker run hello-world
```

# 配置Docker国内镜像

- [参考](http://www.ddrfans.com/Html/1/214987.html)



# 卸载Docker

```bash
# 1. 卸载依赖
yum remove docker-ce docker-ce-cli containerd.io
# 2. 删除资源
rm -rf /var/lib/docker # Docker的默认工作资源路径
```

# 镜像相关命令

- `docker images`：查看所有镜像
  - -a：列出所有镜像
  - -q：只显示镜像的id
  
- 搜索镜像：`docker search` image_name

  - 等效于在[docker官方的镜像仓库](https://hub.docker.com/)搜索

- 下载镜像：`docker pull` image_name[:tag] #tag是标签

- 删除镜像：`docker rmi` image_name/image_id 

  - 查出所有镜像id，递归删除：docker rmi $(docker images -aq) 
  - 强制删除：-f

# 容器相关命令

## 查看容器

```bash
# docker ps             列出正在运行的容器
# docker ps -a          列出当前正在运行的容器+历史运行过的容器
# docker ps -n=?        显示最近创建的容器  ? number  指个数
# docker ps -q          只显示容器的编号
```

## 退出容器

```bash
exit            # 停止容器并退出
Ctrl+P+Q        # 保持容器运行并退出
```

## 删除容器

```bash
docker rm 容器id                   # 删除单个容器
docker rm 容器id 容器id             # 删除多个容器
docker rm -f $(docker ps -aq)       # 删除所有容器
docker ps -aq|xargs docker rm       # 同上
```

## 启动和停止容器

```bash
docker start 容器id       # 启动容器
docker restart 容器id     # 重启容器
docker stop 容器id        # 停止当前正在运行的容器
docker kill 容器id        # 强制停止容器
```

- 使用镜像创建容器并运行：`docker run [ARGS] image_name`
  - 容器别称, 用来区分不同容器：--name="NAME"
  - 后台方式运行：-d  （docker run -d image_name）
  - 使用交互方式运行，进入容器查看内容：-it
  - 指定容器的端口
    - -p ip:主机端口:容器端口
    - -p 主机端口:容器端口(常用)
    - -p 容器端口

```bash
[root@Oskari docker]docker run -it centos:7 /bin/bash
[root@1df2f1c76ad1 /]ls # 查看容器内的centos, 基础版本, 很多命令都不是完善的.
[root@1df2f1c76ad1 /]exit # 从容器中退出主机
```

## docker run 与docker start的区别
- docker run 后面指定的是一个镜像，docker start指定的是一个容器
- docker run 只在第一次运行时使用，通过镜像创建容器，以后再次启动这个容器时，只需要使用命令docker start即可。
- docker run 执行了两步操作：创建容器（docker create），然后将容器启动（docker start）。
- docker start是启动已存在的镜像。

## Docker commit
- docker commit 命令用于提交容器成为一个新的镜像。如果想保存当前容器状态，就可以通过 commit 来提交，获得一个镜像，就好比我们我们使用虚拟机的快照。
- 命令格式（和git原理类似）：docker commit -m="描述信息" -a="作者" 当前容器id 目标镜像名:[TAG]
- 例如：docker commit -a="kuangshen" -m="add webapps app" 容器id tomcat02:1.0

# 其他命令

- 查看版本：docker -v
- 显示 docker 的版本信息：docker verion
- 显示 docker 的系统信息, 包括镜像和容器的数量等：docker info
- 帮助命令：docker COMMAND --help

# 其他命令2

- 查看docker容器运行状态（内存、cpu占用等）：docker stats
- 查看某容器最后10条日志：docker logs -tf --tail 10 容器id
- 查看容器的进程信息：docker top 容器id
- 查看容器的元数据：docker inspect 容器id
- 通过exec进入容器, 并创建一个 /bin/bash 进程：docker exec -it 25d00508c3f9 /bin/bash
- 通过attach进入容器, 进入正在运行的进程, 并不会创建进程：docker attach 25d00508c3f9
- 常见的坑： docker 容器使用后台运行, 就必须要有一个前台进程或执行的程序。docker发现没有应用，就会自动停止

## 容器内拷贝文件到宿主机上

- docker cp 容器id:容器内文件路径 宿主机路径
- 拷贝是一个手动过程, 未来我们可以使用 -v ，即数据卷的技术, 做到数据自动同步

# Docker Portainer可视化

- 执行安装命令：`docker run -d -p 8088:9000 --restart=always -v /var/run/docker.sock:/var/run/docker.sock --privileged=true portainer/portainer`
- 然后访问http://ip:8088/即可

# Docker 镜像加载原理

- docker 的镜像实际上由一层一层的文件系统组成，这种层级的文件系统称为 UnionFS（联合文件系统）。
- bootfs(boot file system）主要包含 bootloader 和 Kernel, bootloader 主要是引导加载 kernel, Docker 刚启动时会加载 bootfs 文件系统，在 Docker 镜像的最底层就是 bootfs。这一层与我们典型的 Linux/Unix 系统是一样的，包含 boot 加载器和内核。加载完成之后整个内核就都在内存中了，此时内存的使用权已由 bootfs 转交给内核，此时系统也会卸载 bootfs。
- rootfs（root file system) 在 bootfs 之上。包含的就是典型 Linux 系统中的 /dev,/proc,/bin,/etc 等标准目录和文件。 rootfs 就是各种不同的操作系统发行版，比如 Ubuntu, Centos 等等。
- Docker安装操作系统为什么小？因为底层直接用 Host 的 kernel，自己只需要提供 rootfs 就可以了。对于不同的 Linux 发行版， bootfs 基本是一致的， rootfs 会有差別，因此不同的发行版可以公用 bootfs.

<img src="./resource/image-20220118214706130.png" alt="image-20220118214706130" style="zoom: 50%;" />

# 对docker容器内核的理解

- Docker 容器本质是宿主机内核的一个隔离进程，通过 Linux 的 **Namespaces**（资源隔离）和 **Cgroups**（资源限制）实现环境隔离。
- 容器内看到的**内核版本**（cpu等）与宿主机完全一致（通过 `uname -r` 可验证）。即使宿主机是 CentOS，容器也可以运行 Ubuntu 用户空间
- 容器启动时无需加载独立内核，直接共享宿主机的内核。
- **影响**：
  - 容器内的应用**必须兼容宿主机内核**（例如，宿主机内核版本过低可能导致容器内应用无法运行）。
  - 宿主机内核的配置（如安全模块、网络参数）直接影响所有容器

# Docker镜像的分层结构

- 所有的 Docker 镜像都起始于一个基础镜像层，当进行修改或培加新的内容时，就会在当前镜像层之上，创建新的镜像层（或者说是容器层，发布之后就称为镜像了），而不会改变基础的镜像层。
- 也就是说Docker 镜像都是只读的，当容器启动时，一个新的可写层加载到镜像的顶部！这一层就是我们通常说的容器层，容器之下的都是镜像层！
- 下图中展示了一个稍微复杂的三层镜像，在外部看来整个镜像只有 6 个文件，这是因为最上层中的文件 7 是文件 5 的一个更新版

<img src="./resource/image-20220117202355233.png" alt="image-20220117202355233" style="zoom: 33%;" />

- 这种情況下，上层镜像层中的文件**覆盖**了底层镜像层中的文件。这样就使得文件的更新版本作为一个新镜像层添加到镜像当中。
- 下图展示了与系统显示相同的三层镜像。所有镜像层堆并合井，**对外提供统一的视图**

<img src="./resource/image-20220117202505750.png" alt="image-20220117202505750" style="zoom: 33%;" />

- 查看镜像分层的方式可以通过命令： `docker image inspect 镜像名/id`

## 补充

- Docker 通过存储引擎（新版本采用快照机制）的方式来实现镜像层堆栈，并保证多镜像层对外展示为统一的文件系统。
- Linux 上可用的存储引撃有 AUFS、 Overlay2、 Device Mapper、Btrfs 以及 ZFS。顾名思义，每种存储引擎都基于 Linux 中对应的文件系统或者块设备技术，井且每种存储引擎都有其独有的性能特点。
- Docker 在 Windows 上仅支持 windowsfilter 一种存储引擎，该引擎基于 NTFS 文件系统之上实现了分层和 CoW [1]。

# Docker安装RabbitMQ

- [docker安装RabbitMQ](https://blog.csdn.net/qq_34775355/article/details/108305396)

# Docker配置下载镜像加速

- [参考](https://blog.csdn.net/weixin_45954060/article/details/121111417)

# Docker 命令流程示意图

<img src="./resource/image-20220119211710692.png" alt="image-20220119211710692" style="zoom: 50%;" />

<img src="./resource/image-20220119211906552.png" alt="image-20220119211906552" style="zoom: 50%;" />
