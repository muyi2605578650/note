![image-20220324122010055](./resource/image-20220324122010055.png)



## Docker中Jenkins的安装

- 安装：

```shel
docker pull jenkins/jenkins:lts
```

- 运行（Linux下）：

```she
docker run -p 8080:8080 -p 50000:5000 --name jenkins \
-u root \
-v /mydata/jenkins_home:/var/jenkins_home \
-d jenkins/jenkins:lts
```

- 运行（win10下）：（宿主机目录为D盘下）
- 挂载不对的话，可能导致重启Jenkins后配置丢失哦

```shell
docker run -p 8080:8080 -p 50000:5000 --name jenkins -u root -v /mydata/jenkins_home:/D/PACKAGE/env/docker_jenkins_volumn -d jenkins/jenkins:lts
```

## Jenkins+Gitee配置自动构建

- [jenkins gitee 自动构建项目](https://blog.csdn.net/feifei10244499/article/details/105708299) 
- [Jenkins + Gitee 实现代码自动化构建 （超级详细）](https://cloud.tencent.com/developer/article/2024230)
- 注意gitee的webhook密码要在Jenkins上生成，在`Secret Token for Gitee WebHook`那里
