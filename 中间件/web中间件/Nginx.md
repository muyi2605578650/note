# Keepalived简介
- [参考](https://blog.csdn.net/li281037846/article/details/122931666)

# Nginx高可用搭建环境说明

- 在docker中模拟内网环境，两台ubuntu主机，master是172.17.0.2，backup是172.17.0.3，每台各有一个nginx+keepalive，keepalived设置的虚拟ip是172.17.0.10
- 对于ubuntu版本，我是直接docker pull的，对于nginx和keepalived版本，也是直接apt-get install
- ip不用和我一样，相互能通就行

# Docker中pull并运行Ubuntu

- docker pull ubuntu
- 运行master主机：docker run -dit -p 81:80 --privileged --name=myubuntu81 ubuntu /bin/bash
- 运行backup主机：docker run -dit -p 82:80 --privileged --name=myubuntu82 ubuntu /bin/bash
  - 说明：81和82是我暴露到宿主机的端口（其实也不用，内网用curl测试也行），--privileged是指以特权模式启动（否则keepalived无法成功生成虚拟ip）
- 进入容器：docker exec -it myubuntu81 /bin/bash

# 安装工具

- 刚pull下来的ubuntu很干净，需要安装一些工具

```shell
apt-get update
#几个必备工具：
apt-get install vim
apt-get install net-tools
apt-get install systemctl
apt-get install curl
apt-get install psmisc #后面脚本killall命令要用到

#ps：如果是官网下载压缩包安装keepalived，需要先安装以下依赖（我是直接apt-get安装，这一步略过）：
apt-get install gcc
apt-get install libssl-dev 
apt-get install openssl 
apt-get install libpopt-dev 
apt-get install libnl-3-dev
apt-get install libnl-genl-3-dev
apt-get install make
```

# 安装Nginx和Keepalived

```shell
apt-get install keepalived
apt-get install nginx
```

- 安装完成后，修改nginx的默认页面，使得两台主机的nginx页面不一样就行，方便查看一会儿的主备切换效果

# 配置keepalived

- 刚安装的keepalived没有默认配置文件，需要在/etc/keepalived/目录下创建keepalived.conf文件
- keepalived -t：测试配置文件是否有问题

## Master配置

- router_id：代表本机ip
- script_user：执行脚本的用户
- interval：间隔几秒执行脚本（注意最好大于脚本的执行时间）
- interface：网卡名称
- virtual_ipaddress：虚拟ip

```config
! Configuration File for keepalived

global_defs {
    router_id localhost
    script_user root
    enable_script_security
}

vrrp_script chk_http_port {
    script /etc/keepalived/check_nginx.sh
    interval 10
    weight -20
}

vrrp_instance VI_1 {
    state MASTER
    interface eth0
    virtual_router_id 66
    priority 100
    advert_int 1
    authentication {
       auth_type PASS
       auth_pass 1111
    }
    track_script {
       chk_http_port
    }
    virtual_ipaddress {
       172.17.0.10
    }
}
```

## Backup配置

```config
! Configuration File for keepalived

global_defs {
    router_id localhost
    script_user root
    enable_script_security
}

vrrp_script chk_http_port {
    script "/etc/keepalived/check_nginx.sh"
    interval 10
    weight -20
}

vrrp_instance VI_1 {
    state BACKUP
    interface eth0
    virtual_router_id 66
    priority 90
    advert_int 1
    authentication {
       auth_type PASS
       auth_pass 1111
    }
    track_script {
       chk_http_port
    }
    virtual_ipaddress {
       172.17.0.10
    }
}
```



## check_nginx.sh脚本

- 脚本路径：`/etc/keepalived/check_nginx.sh`
- 权限设置为755，否则keepalived会认为它不安全
- 注意首行一定是`#!/bin/bash`，而不是#/bin/bash，否则脚本不会被keepalived执行
- 脚本内容：就是查看是否存在nginx进程，不存在就重启nginx，然后再查看一次，还不存在就杀掉所有keepalived进程（这样backup就会自动上线）
- 脚本日志：/etc/keepalived/check_nginx.log
- 脚本两台机器都要有

```shell
#!/bin/bash
echo  $(date)  " start check nginx..." >> /etc/keepalived/check_nginx.log
A=`ps -C nginx --no-header | wc -l`
if [ $A -eq 0 ];then
        echo $(date)  "nginx is not running, restarting..." >> /etc/keepalived/check_nginx.log
            systemctl restart nginx
                if [ `ps -C nginx --no-header | wc -l` -eq 0 ];then
                        echo $(date) "nginx is down, kill all keepalived..." >> /etc/keepalived/check_nginx.log
                        killall keepalived
                                    fi
fi
```

# 高可用测试

1. 启动两台nginx：`systemctl start nginx`
2. 先启动master的keepalived，再启动slave的keepalived：`systemctl start keepalived`
3. 在master上执行`ip addr`，可以看到有虚拟ip生成，而此时backup上还没有
4. 在两台机器上都执行一下：`curl 172.17.0.10`，发现返回的是master的nginx页面
5. 故意改错master上的nginx配置文件，然后停止nginx：`systemctl stop nginx`
6. 此时keepalived的脚本会检测到nginx进程没了，尝试重启，但nginx配置文件是错的，所以重启失败，最终脚本会杀掉keepalived，此时虚拟ip将会转移到backup
7. 在backup上执行`ip addr`，可以看到虚拟ip了
8. 在两台机器上都执行：`curl 172.17.0.10`，发现此时返回的已经是backup的nginx页面了
9. 高可用测试成功！
10. 最后注意一点：我通过测试发现此时如果启动master的nginx和keepalived，虚拟ip又会转移到master上

# 总结

- 这只是最基础的nginx高可用，只有一主一备
- 在此基础上，可以开始搭建nginx负载均衡、反向代理等等
- 参考：[**Keepalived 的原理和 web 服务高可用实践**](https://m.zhipin.com/mpa/html/get/column?contentId=7f87319981e462f9qxB-2dy4)

# Nginx原理

![image-20220316183214960](./resouce/image-20220316183214960.png)

## master-workers 的机制的好处

首先，对于每个 worker 进程来说，独立的进程，不需要加锁，所以省掉了锁带来的开销，同时在编程以及问题查找时，也会方便很多。

其次，采用独立的进程，可以让互相之间不会影响，一个进程退出后，其它进程还在工作，服务不会中断， master 进程则很快启动新的 worker 进程。

当然， worker 进程的异常退出，肯定是程序有 bug 了，异常退出，会导致当前 worker 上的所有请求失败，不过不会影响到所有请求，所以降低了风险。

## 需要设置多少个 worker

Nginx 同 redis 类似都采用了 io 多路复用机制，每个 worker 都是一个独立的进程，但每个进程里只有一个主线程，通过异步非阻塞的方式来处理请求， 即使是千上万个请求也不在话下。每个 worker 的线程可以把一个 cpu 的性能发挥到极致。

**所以 worker 数和服务器的 cpu 数相等是最为适宜的**。设少了会浪费 cpu，设多了会造成 cpu 频繁切换上下文带来的损耗。

## 连接数 worker_connection

这个值是表示每个 worker 进程所能建立连接的最大值，所以，一个 nginx 能建立的最大连接数，应该是 `worker_connections * worker_processes`。当然，这里说的是最大连接数，对于 HTTP 请求本地资源来说，能够支持的最大并发数量是`worker_connections * worker_processes`

如果是支持 http1.1 的浏览器每次访问要占两个连接，所以普通的静态访问最大并发数是：`worker_connections * worker_processes /2`，而如果是 HTTP 作 为反向代理来说，最大并发数量应该是`worker_connections * worker_processes/4`。因为作为反向代理服务器，每个并发会建立与客户端的连接和与后端服务的连接，会占用两个连接。