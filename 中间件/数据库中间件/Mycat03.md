# HAProxy和KeepAlived简介

- 在实际项目中， Mycat 服务也需要考虑高可用性，如果 Mycat 所在服务器出现宕机，或 Mycat 服务故障，需要有备机提供服务，需要考虑 Mycat 集群
- HAProxy 实现了 MyCat 多节点的集群高可用和负载均衡， 而 HAProxy 自身的高可用则可以通过 Keepalived 来实现
- HAProxy类似于Nginx

# Mycat配置user权限

- server.xml配置user的权限为只读

```xml
<user name="user">
	<property name="password">user</property>
	<property name="schemas">TESTDB</property>
	<property name="readOnly">true</property>
</user>
```

- privileges 标签可以对逻辑库（schema）、表（table）进行精细化的 DML 权限控制。
  - check 属性，如为 true 开启权限检查，为 false 不开启，默认为 false

```xml
#配置orders表没有增删改查权限
<user name="mycat">
	<property name="password">123456</property>
	<property name="schemas">TESTDB</property>
	<!-- 表级 DML 权限设置 四位数分别代表增改查删，0为禁止 -->
	<privileges check="true">
		<schema name="TESTDB" dml="1111" >
			<table name="orders" dml="0000"></table>
			<!--<table name="tb02" dml="1111"></table>-->
		</schema>
	</privileges>
</user>
```

- 配置说明：

| DML 权限 | 增加（insert） | 更新（update） | 查询（select） | 删除（select） |
| -------- | -------------- | -------------- | -------------- | -------------- |
| 0000     | 禁止           | 禁止           | 禁止           | 禁止           |
| 0010     | 禁止           | 禁止           | 可以           | 禁止           |
| 1110     | 可以           | 禁止           | 禁止           | 禁止           |
| 1111     | 可以           | 可以           | 可以           | 可以           |

# Mycat配置白名单和黑名单

- 白名单实现某主机某用户可以访问 Mycat，而其他主机用户禁止访问
- 黑名单用于设置某类SQL执行的权限
- server.xml配置文件：

```xml
#配置只有192.168.140.128主机可以通过mycat用户访问
<firewall>
	<whitehost>
		<host host="192.168.140.128" user="mycat"/>
	</whitehost>
    <blacklist check="true">
		<property name="deleteAllow">false</property>
	</blacklist>
</firewall>
```

- 可以设置的黑名单 SQL 拦截功能列表：

| 配置项           | 缺省值 | 描述                          |
| ---------------- | ------ | ----------------------------- |
| selelctAllow     | true   | 是否允许执行 SELECT 语句      |
| deleteAllow      | true   | 是否允许执行 DELETE 语句      |
| updateAllow      | true   | 是否允许执行 UPDATE 语句      |
| insertAllow      | true   | 是否允许执行 INSERT 语句      |
| createTableAllow | true   | 是否允许创建表                |
| setAllow         | true   | 是否允许使用 SET 语法         |
| alterTableAllow  | true   | 是否允许执行 Alter Table 语句 |
| dropTableAllow   | true   | 是否允许修改表                |
| commitAllow      | true   | 是否允许执行 commit 操作      |
| rollbackAllow    | true   | 是否允许执行 roll back 操作   |

# Mycat监控平台Mycat-Web

- Mycat-web 是 Mycat 可视化运维的管理和监控平台，弥补了 Mycat 在监控上的空白。帮 Mycat 分担统计任务和配置管理任务
  - Mycat-web 主要管理和监控 Mycat 的流量、连接、活动线程和内存等，具备 IP 白名单、邮件告警等模块，还可以统计 SQL 并分析慢 SQL 和高频 SQL 等
- Mycat-web 引入了 ZooKeeper 作为配置中心，所以需要连接zk才可使用
- 安装后访问http://ip:8082/mycat/即可

# 参考

- [Mycat的使用_尚硅谷视频学习笔记](https://blog.csdn.net/qq_17079255/article/details/108944499)
