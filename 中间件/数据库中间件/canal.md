官方文档：[点击](https://github.com/alibaba/canal)

# canal是什么

- 基于 MySQL 数据库增量日志解析，提供增量数据订阅和消费
- 包括
  - 数据库镜像
  - 数据库实时备份
  - 索引构建和实时维护(拆分异构索引、倒排索引等)
  - 业务 cache 刷新
  - 带业务逻辑的增量数据处理
- 当前的 canal 支持源端 MySQL 版本包括 5.1.x , 5.5.x , 5.6.x , 5.7.x , 8.0.x

# canal 工作原理

- canal 模拟 MySQL slave 的交互协议，伪装自己为 MySQL slave ，向 MySQL master 发送dump 协议
- MySQL master 收到 dump 请求，开始推送 binary log 给 slave (即 canal )
- canal 解析 binary log 对象(原始为 byte 流)

# canal server配置项说明

[点击](https://github.com/alibaba/canal/wiki/AdminGuide)