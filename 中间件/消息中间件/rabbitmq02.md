> 注意：同一个交换机或队列的声明位置可以在消费者，也可以在生产者，也可以重复声明，如果使用了SpringBoot整合后，就可统一配置到配置类中

# RabbitMQ的5种运行模式

1. 简单队列模式
   1. 使用默认交换机
   2. 1个生产者+1个消费者
2. worker模式
   1. 使用默认交换机
   2. 1个生产者，多个消费者
   3. 单个消息只能被1个消费者消费
   4. 多个消息默认是类似轮询的方式发给多个消费者
3. 订阅模式（fanout）
   1. 生产者数量>=1，消费者数量>1
   2. 每一个消费者都有自己的一个队列
   3. 生产者发送的消息，经过交换机，到达队列，实现一个消息被多个消费者获取的目的
4. 路由模式（direct）
   1. 同订阅模式，区别是：不再广播，而是根据routingKey路由到指定的消费者
   2. 多个队列的routingKey可以是一样的，这样就变成了fanout
5. 主题模式（topic）
   1. 同路由模式，区别是：routingKey添加了通配符概念
   2. *可以代替一个单词，#可以替代零个或多个单词
      1. 最后一个单词是 rabbit 的 3 个单词 `(*.*.rabbit)`
      2. 第一个单词是 lazy 的多个单词 `(lazy.#)`
   3. 当一个队列routingKey是#，那么这个队列将接收所有数据，变成 fanout 了
   4. 如果routingKey没有#和*出现，那么就变成 direct 了

参考：[RabbitMQ的五种工作模式](https://blog.csdn.net/reed1991/article/details/53394906)

# 简单队列模式

- worker模式也可以用此代码测试
- 生产者核心代码：

```java
//获取信道
Channel channel = connection.createChannel();
/**
 * 生成一个队列
 * 1.队列名称
 * 2.队列是否持久化
 * 3.是否排他 该队列是否只供一个消费者进行消费 是否进行共享 true 可以多个消费者消费（待研究）
 * 4.是否自动删除 最后一个消费者端开连接以后 该队列是否自动删除 true 自动删除
 * 5.其他参数
 */
channel.queueDeclare(QUEUE_NAME, false, false, false, null);
/**
 * 发送一个消息
 * 1.发送到那个交换机
 * 2.routingKey 路由的 key 是哪个
 * 3.其他的参数信息
 * 4.发送消息的消息体
 */
channel.basicPublish("", QUEUE_NAME, null, message.getBytes());

```

- 消费者核心代码：

```java
Channel channel = connection.createChannel();
/**
 * 消费者消费消息 - 接受消息
 * 1.消费哪个队列
 * 2.消费成功之后是否要自动应答 true 代表自动应答 false 手动应答
 * 3.消费者未成功消费的回调
 * 4.消息被取消时的回调
 */
channel.basicConsume(QUEUE_NAME, true, deliverCallback, cancelCallback);
```

# 发布-订阅模式（fanout）

```java
//生产者核心代码
channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
channel.basicPublish(EXCHANGE_NAME, "", null, message);

//消费者核心代码
channel.queueBind(queueName, EXCHANGE_NAME, "");//fanout模式，和routingKey无关
channel.basicConsume(queueName, true, deliverCallback, cancelCallback);
```

# 路由模式（direct）

```java
//生产者核心代码
channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
channel.basicPublish(EXCHANGE_NAME, routingKey, null, message);

//消费者核心代码
channel.queueBind(queueName, EXCHANGE_NAME, "error");//指定routingKey=error
channel.basicConsume(queueName, true, deliverCallback, cancelCallback);
```

# 主题模式（topic）

```java
//生产者核心代码
channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
channel.basicPublish(EXCHANGE_NAME, routingKey, null, message);
//消费者核心代码
channel.queueDeclare(queueName, false, false, false, null);
channel.queueBind(queueName, EXCHANGE_NAME, "*.orange.*");
channel.basicConsume(queueName, true, deliverCallback, cancelCallback);
```

# 路由模式——SpringBoot方式整合

- 配置类

```java
/**
 * 构建DirectExchange交换机
 */
@Bean
public DirectExchange directExchange() {
    // 支持持久化
    return new DirectExchange(DIRECT_EXCHANGE_NAME, true, false);
}

/**
* 构建队列
*/
@Bean
public Queue test1Queue() {
    // 支持持久化
    return new Queue(QUEUE_TEST1, true);
}

/**
* 绑定交换机和队列
*/
@Bean
public Binding test1Binding(DirectExchange directExchange, Queue test1Queue) {
    //这样写spring会自动按名称注入
    return BindingBuilder.bind(test1Queue).to(directExchange).with(ROUTING_KEY_TEST1);//指定routingKey
}


/**
* 配置ConnectionFactory（也可不配置，默认localhost）
*/
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory("127.0.0.1", 5672);
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setPublisherConfirmType(CachingConnectionFactory.ConfirmType.SIMPLE);//设置发布确认类型
        return connectionFactory;
    }

/**
* 实例化操作模板
*/
@Bean
public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
    return new RabbitTemplate(connectionFactory);
}
```

- 生产者
- rabbitTemplate还有很多send方法，如：
  - sendAndReceive();
  - convertAndSend();
  - convertSendAndReceive();
  - convertSendAndReceiveAsType();

```java
MessageProperties messageProperties = new MessageProperties();
messageProperties.setDeliveryMode(MessageDeliveryMode.PERSISTENT);//消息持久化
Message message = new Message("你好,这是发给test1队列的消息".getBytes(), messageProperties);
rabbitTemplate.send(DIRECT_EXCHANGE_NAME, ROUTING_KEY_TEST1, message);
```

- 消费者

```java
/**
 * 监听test1队列
 */
@RabbitListener(queues = QUEUE_TEST1)
public void consumeMessage1(Message message) {
    System.out.println("test1队列收到：======" + new String(message.getBody(), StandardCharsets.UTF_8));
}
```


# 完整代码

- 完整代码：[GitHub](https://github.com/StellarX/Java_coding_practice/tree/main/rabbitmq-study)
