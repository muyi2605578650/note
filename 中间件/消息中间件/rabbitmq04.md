# 备份交换机

- 对于已经到达队列但无法被正常消费的消息，可以设置死信队列来存储这些消息
- 但是对于不可路由的消息，**根本没有机会进入到队列**，因此无法使用死信队列
- 在 RabbitMQ 中，有一种备份交换机的机制存在，可以很好的应对这个问题
- 备份交换机就是用于处理`无法被普通交换机路由`的消息
- 需要注意：当同时设置了消息回退机制和备份交换机时，会优先采用备份交换机
- 下图中，无法路由的消息将通过备份交换机发送给告警队列，示意图：

![image-20220324184025417](./resource/image-20220324184025417.png)

- 核心配置：

```java
//声明确认交换机的备份交换机
@Bean
public DirectExchange confirmExchange() {
    ExchangeBuilder exchangeBuilder = ExchangeBuilder.directExchange(CONFIRM_EXCHANGE_NAME)
        .durable(true)
        .alternate(BACKUP_EXCHANGE_NAME);//设置该交换机的备份交换机
        //.withArgument("alternate-exchange", BACKUP_EXCHANGE_NAME);
    return exchangeBuilder.build();
}
```

- 告警队列消费者：

```java
@RabbitListener(queues = WARNING_QUEUE_NAME)
public void receiveWarningMsg(Message message) {
    String msg = new String(message.getBody());
    log.warn("报警发现不可路由消息：{}", msg);
}
```

# 优先队列

- 是指队列中的消息具备优先级，优先级高的消息将先被消费
- 使用场景：订单催付，大客户先催付
- 创建队列时指定参数：`QueueBuilder.durable(WARNING_QUEUE_NAME).maxPriority(10).build()`（优先级在0-255之间）
- 发送消息时指定优先级即可

# 惰性队列

- 惰性队列会尽可能的将消息存入`磁盘`中，而在消费者消费到相应的消息时才会被加载到内存中
- 当消费者由于各种各样的原因(比如消费者下线、宕机亦或者是由于维护而关闭等)而致使长时间内不能消费消息造成堆积时，惰性队列就很有必要了
- 在创建队列时设置即可：`QueueBuilder.durable(BACKUP_QUEUE_NAME).lazy().build();`
- MQ从 3.6.0 版本引入了惰性队列的概念

# Federation exchange 和 shovel

- 有空再学
