# 集群搭建

- 比较简单，有空再尝试搭建，大概步骤如下
- 准备3台rabbitmq
- 保证相互之间可以ping通，最好修改hostname便于分辨
- 复制 erlang 的 cookie（RabbitMQ 集群需要在每个从节点上使用与主节点一样的 ErLang Cookie），参考如下：

```shell
scp /var/lib/rabbitmq/.erlang.cookie root@node2:/var/lib/rabbitmq/.erlang.cookie
scp /var/lib/rabbitmq/.erlang.cookie root@node3:/var/lib/rabbitmq/.erlang.cookie
```

- 重启3台mq
- node2执行相关命令加入node1，node3加入node2
- 任意一台mq上查看集群状态，ok
- 创建账户、设置角色、权限等
- 解除某个节点，执行相关命令即可

# 配置镜像队列

- 默认搭好的集群，每个节点之间，数据是不同步的，需要配置镜像队列
- 在管理界面添加策略：保持集群中始终有2台机器数据自动同步，所有创建的队列必须带上指定前缀才生效

<img src="./resource/image-20220324221918764.png" alt="image-20220324221918764" style="zoom: 50%;" />

# Nginx+Keepalived实现MQ负载均衡+HA

- MQ默认不支持负载均衡，需要配合Nginx或HAProxy实现负载均衡，如果要两台Nginx，则还需要使用Keepalived实现Nginx的HA以及虚拟IP，最终用户通过虚拟IP就可以负载均衡到所有MQ节点
- 有空再实战，架构图如下：

<img src="./resource/image-20220324222824491.png" alt="image-20220324222824491" style="zoom: 45%;" />