> [参考教程](https://www.bilibili.com/video/BV1cb4y1o7zz)
>
> [其他参考文章](https://juejin.cn/post/6997217228743507981)
>
> [参考笔记](https://note.oddfar.com/rabbitmq/#%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B)

# MQ基本概念

- ConnectionFactory（连接管理器）：应用程序与Rabbit之间建立连接的管理器，程序代码中使用。
- Channel（信道）：消息推送使用的通道。
- Exchange（交换机）：用于接受、分配消息。
  - 生产者生产的消息从不会直接发送到队列，而是先发送到交换机，交换机的类型决定了它如何处理消息
  - 默认交换机用`空字符串`表示
- Queue（队列）：用于存储生产者的消息。
- RoutingKey（路由键）：用于将消息从交换机路由到某个队列
  - 默认交换机情况下，routingKey就是队列名称，如：channel.basicPublish("", `QUEUE_NAME`, null, message.getBytes());
- BindingKey（绑定键）：实际代码中并没有bindingKey，它和routingKey是一个东西，只是为了理解交换机和队列的绑定关系而生（bindingKey是交换机和队列建立关系时候的key，而routingKey是生产者发送消息时候的key，交换机根据routingKey匹配名称相同的bindingKey，然后将消息发给对应的队列）

![image-20220320212804367](./resource/image-20220320212804367.png)

# 消息应答机制

- 是指：**消费者在接收到消息并且处理该消息之后，告诉 rabbitmq 它已经处理了，rabbitmq 可以把该消息删除了**
- 自动应答：是指消费者接收到消息就应答，不可取（除非能保证消息基本不会丢失），后续如果业务代码处理失败，相当于消息就丢失了
- 手动应答：
  - Channel.basicAck 用于肯定确认
  - Channel.basicNack 用于否定确认
  - Channel.basicReject 用于否定确认，可以将其丢弃
- 手动应答代码如下：

```java
DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody());
            System.out.println(message);
            /**
             * 1.消息标记 tag
             * 2.是否批量应答未应答消息
             */
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
        };
//······
//第二个参数就是指不自动应答
channel.basicConsume(QUEUE_NAME, false, deliverCallback, cancelCallback);
```

# 临时队列

- 没有持久化的队列就属于临时队列
- 创建临时队列：`String queueName = channel.queueDeclare().getQueue();`

# 队列持久化

- 生产者创建队列的时候指定即可：

```java
channel.queueDeclare(QUEUE_NAME, true, false, false, null);
```

# 消息持久化

- 生产者发送消息时指定：
- 持久化后的队列在MQ管理界面会带有一个D字母

```java
channel.basicPublish("", QUEUE_NAME, MessageProperties.PERSISTENT_BASIC, message.getBytes());
```

# 设置prefetchCount

- 通过`channel.basicQos(prefetchCount)`设置（该方法还有另外两个可选参数，有空再研究）
- 注意是在`消费者`端设置，且默认只针对该消费者
- 设置生效的前提是`开启手动应答`
- 说一下对prefetchCount的理解：
  - 比如设置为5，就类似在消费者端有个队列用于预存消息，列队的长度就是预取值5，如果消费者处理的慢，消息会积压在队列中，最多就只能积压5个；如果处理的快，那么队列就会一直有空闲，消费者就可以源源不断的分发到消息
  - 如果不设置，默认=0，也就是不预存消息，消息直接发给消费者
- 一般来说，设置预取，会导致消费者接收到的消息变少，其实就是限流了
- 收到消息后，业务处理完成，必须手动应答，否则，这个消息会占用队列，如果都不应答，队列占满了，就再也无法收到消息了

## 案例分析

假设现在有1个生产者向队列发消息，2个消费者（C1、C2）接收消息

- 情况1：C1、C2都不设置预取，消息会直接发给消费者，因为有2个消费者，所以用轮询（worker模式）的方式发给它们（和C1、C2处理消息的速度无关），自然这就属于公平的分发
- 情况2：C1、C2都设置预取值=n（n>=1），n具体是多少无所谓，可以假设n=1，此刻由于存在一个长度=1的预存队列
  - 如果C1、C2处理消息速度相同，那么他们收到的消息数量将会基本相同
  - 如果速度不相同，那么速度快的会收到更多消息，自然就成了不公平的分发
- 情况3：C1不设置预取，C2设置预取值=n（n>=1），这时C1会收到更多数据，因为C1没有队列存在，不限制消息数量（和处理速度无关）

## 总结

- 虽然自动应答传输消息速率是最佳的，但是，在这种情况下已传递但尚未处理的消息的数量也会增加，从而增加了消费者的内存消耗。
- 应该小心使用具有无限预处理的自动确认模式或手动确认模式，消费者消费了大量的消息如果没有确认的话，会导致消费者连接节点的内存消耗变大
- 所以找到合适的预取值是一个反复试验的过程，不同的负载该值取值也不同 100 到 300 范 围内的值通常可提供最佳的吞吐量，并且不会给消费者带来太大的风险。

# 发布-确认机制

- 发布-确认机制是为了`保证消息不会丢失`（因为有持久化还是可能丢失消息，比如在生产者发送消息到队列的途中丢失）
- 生产者开启发布-确认后，一旦消息被`投递到队列`之后，broker 就会发送一个确认给生产者(包含消息的唯一 ID)，告诉生产者消息已经到达队列
- 如果消息和队列是可持久化的，那么确认消息会在`将消息写入磁盘`之后发出
- 开启发布确认：`channel.confirmSelect();`
- 等待确认：`channel.waitForConfirms();`
- 添加异步确认监听器：`channel.addConfirmListener(ackCallback, nackCallback);`

## 分类

- 单个发布确认：同步等待确认，简单，但吞吐量非常有限。
- 批量发布确认：批量同步等待确认，简单，合理的吞吐量，但出现问题无法推断出是哪条消息出问题
- 异步发布确认：最佳性能和资源使用，在出现错误的情况下可以很好地控制

## 单个发布确认

```java
channel.queueDeclare(queueName, true, false, false, null);
//开启发布确认
channel.confirmSelect();

for (int i = 0; i < MESSAGE_COUNT; i++) {
    String message = i + "";
    channel.basicPublish("", queueName, null, message.getBytes());
    //服务端返回 false 或超时时间内未返回，生产者可以消息重发
    boolean flag = channel.waitForConfirms();
    if (flag) {
        System.out.println("消息发送成功");
    }
}
```

## 批量发布确认

```java
channel.queueDeclare(queueName, true, false, false, null);
//开启发布确认
channel.confirmSelect();
//批量确认消息大小
int batchSize = 100;
//未确认消息个数
int outstandingMessageCount = 0;

for (int i = 0; i < MESSAGE_COUNT; i++) {
    String message = i + "";
    channel.basicPublish("", queueName, null, message.getBytes());
    outstandingMessageCount++;
    if (outstandingMessageCount == batchSize) {
        channel.waitForConfirms();
        outstandingMessageCount = 0;
    }
}
//为了确保还有剩余没有确认消息 再次确认
if (outstandingMessageCount > 0) {
    channel.waitForConfirms();
}
```

## 异步发布确认

```java
Channel channel = connection.createChannel();
channel.queueDeclare(queue_name, true, false, false, null);

//开启单个发布确认模式
channel.confirmSelect();

//消息监听器
/**
 * 1.监听哪些消息成功了进行回调
 * 2.监听哪些消息失败了进行回调
 */
channel.addConfirmListener(ackCallback, nackCallback);

//批量发送消息
for (int i = 1; i <= 10; i++) {
    String message = i + "";
    System.out.println("send: " + message);
    channel.basicPublish("", queue_name,
                         MessageProperties.PERSISTENT_BASIC, message.getBytes());
    map.put((long) i, message);
}
```

# 发布-确认高级部分+消息回退机制

- 发布确认高级和前面的差不多
- 消息回退：`rabbitTemplate.setReturnsCallback(myCallBack);`，同时配置`spring.rabbitmq.publisher-returns=true`
- [参考](https://note.oddfar.com/pages/c94906/#%E5%8F%91%E5%B8%83%E7%A1%AE%E8%AE%A4-springboot-%E7%89%88%E6%9C%AC)

# 幂等性问题

- 为了防止消息重复消费，服务端一般还要考虑幂等性问题，可以使用redis分布式锁