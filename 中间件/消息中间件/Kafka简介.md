## Kafka和RabbitMQ对比 -todo

- RabbitMQ`实时性、严谨性`比Kafka高，Rabbitmq在金融场景中经常使用
- Kafka的`吞吐量`比RabbitMQ高，Kafka每秒请求数达到`数十万量级`，RabbitMQ每秒请求数则为`万级别`，有测试报告指出Kafka是RabbitMQ的10倍以上性能
- Kafka支持流处理，不支持死信、优先队列
- Kafka依赖zookeeper



## 分区

## 分区器

## 分区键



![image-20220427200609932](./resource/image-20220427200609932.png)