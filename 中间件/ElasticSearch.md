# 参考

> [狂神es笔记](https://www.kuangstudy.com/bbs/1354069127022583809)
>
> [Spring Boot整合Elasticsearch，最新最全教程](https://blog.csdn.net/gybshen/article/details/111469217)

[TOC]

# ELK是什么

- ELK是一套针对日志数据（实际上任何数据分析和收集的场景都可以）做解决方案的框架，分别代表了三款产品： 
  - E: ElasticSearch，负责日志的存储和检索
  - L：Logstash，负责日志的收集，过滤和格式化
  - K：Kibana，负责日志的展示统计和数据`可视化`
- [ELK学习总结——我们为什么要用ELK](https://blog.csdn.net/qiushisoftware/article/details/100298046)

# Lucene


# Solr和ES的比较
- 共同点：
  - 都是基于`Lucene`实现
- 不同点：
  - solr利用`zookeeper`进行分布式管理，而es`自带分布式协调管理`
  - solr比es实现`更加全面`，`功能更多`，而es本身`更注重核心功能`，高级功能多由第三方`插件`提供
  - solr在`传统的搜索`应用中表现好，而es在`实时`搜索方面表现好。当`单纯对已有数据搜索`时，solr更快，`数据量增加、实时建立索引`时，solr就不行了



# ES核心概念

- 默认就是一个集群，一个集群至少有一个节点，比如单机就是一个节点
- 创建索引会有5个分片，不同分片可以放到不同节点上，每个分片实际上就是lucene索引

---

- 索引（ElasticSearch）
- 字段类型（映射）：字段是整型，还是字符型
- 文档
- 分片（Lucene索引，倒排索引）

---

ES中的概念和关系型数据库的对应：

| 索引             | 数据库       |
| ---------------- | ------------ |
| 类型（type）     | Table 数据表 |
| 文档（Document） | Row 行       |
| 字段（Field）    | Columns 列   |

## 常用字段类型

- keyword和text：都表示字符串，主要区别在于：
  - keyword：不会被分词，适用于只有整体索引才有意义的字符串，如：电子邮件、商品分类名称
  - text：会被分词，适用于长文本，如：商品描述、商品名称

# ES安装、运行

- 下载、解压即可，进入`bin`目录，点击`elasticsearch.bat`运行，需要java环境
- Ubuntu安装[参考](https://www.jianshu.com/p/f239520f21f8)

# elasticsearch-head可视化

- 可以去下载[点这里](https://github.com/mobz/elasticsearch-head)，需要nodejs环境
- 谷歌浏览器也可以`直接安装同名插件`

# 安装kibana

- 下载、解压，进入bin目录，点击`kibana.bat`运行，访问localhost:`5601`
- 配置中文界面：进入`/config/kibana.yml`，配置：`i18n.locale: "zh-CN"`

# IK分词器(es插件)

- [下载](https://github.com/medcl/elasticsearch-analysis-ik/releases)、解压到`plugin/ik`目录下
- `/bin/elasticsearch-plugin` 可以查看插件
- 分词器类型：
  - ik_smart：最少切分
  - ik_max_word：最细粒度划分（穷尽词库的可能）
- 自定义的词需要手动添加到扩展词典中（`/plugins/ik/config/IKAnalyzer.cfg.xml`）

---

使用kabana进行分词测试

```shell
GET _analyze
{
  "analyzer": "ik_smart",
  "text": "坏人耗子尾汁"
}

GET _analyze
{
  "analyzer": "ik_max_word",
  "text": "坏人耗子尾汁"
}

-结果如下：

# GET _analyze
{
  "tokens" : [
    {
      "token" : "坏人",
      "start_offset" : 0,
      "end_offset" : 2,
      "type" : "CN_WORD",
      "position" : 0
    },
    {
      "token" : "耗子尾汁",
      "start_offset" : 2,
      "end_offset" : 6,
      "type" : "CN_WORD",
      "position" : 1
    }
  ]
}

# GET _analyze
{
  "tokens" : [
    {
      "token" : "坏人",
      "start_offset" : 0,
      "end_offset" : 2,
      "type" : "CN_WORD",
      "position" : 0
    },
    {
      "token" : "耗子尾汁",
      "start_offset" : 2,
      "end_offset" : 6,
      "type" : "CN_WORD",
      "position" : 1
    },
    {
      "token" : "耗子",
      "start_offset" : 2,
      "end_offset" : 4,
      "type" : "CN_WORD",
      "position" : 2
    },
    {
      "token" : "尾",
      "start_offset" : 4,
      "end_offset" : 5,
      "type" : "CN_CHAR",
      "position" : 3
    },
    {
      "token" : "汁",
      "start_offset" : 5,
      "end_offset" : 6,
      "type" : "CN_CHAR",
      "position" : 4
    }
  ]
}
```

## 分词查询语法

[参考](https://www.cnblogs.com/bonelee/p/6105394.html)

# 关于索引、文档的RESTFUL操作

- 格式：`localhost:9200/索引名称/类型名称/文档id`

---

- 创建文档
```shell
类型是space
POST test1/space/1
{
  "name" : "张三",
  "age" : 10
}

类型是doc
POST /idea/_doc
{
  "content": "让人笑话了不好意思了哈哈哈哈"
}
```
- 更新一个文档（更新不要用PUT，因为没有指定的字段会被置空，而POST不会）

```shell
POST test1/space/1/_update
{
  "doc":{
    "name":"李四"
  }
}
```

- 查询索引信息

```shell
GET test1
```



- more_like_this查询

```shell
POST /idea/_search
{
  "query": {
    "more_like_this": {
      "fields": [
        "content"
      ],
      "like": "编程开心的笑话",
      "min_term_freq": 1,
      "max_query_terms": 12, 
      "min_doc_freq": 0,
      "minimum_should_match": 0.3
      , "percent_terms_to_match": 0.3
    }
  }
}
```

# ES的Java客户端测试

## 新增文档
```java
    //测试添加文档(先创建一个User实体类，添加fastjson依赖)
    @Test
    public void testAddDocument() throws IOException {
        // 创建一个User对象
        User liuyou = new User("liuyou", 18);
        // 创建请求
        IndexRequest request = new IndexRequest("liuyou_index");
        // 制定规则 PUT /liuyou_index/_doc/1
        request.id("1");// 设置文档ID
        request.timeout(TimeValue.timeValueMillis(1000));// request.timeout("1s")
        // 将我们的数据放入请求中
        request.source(JSON.toJSONString(liuyou), XContentType.JSON);
        // 客户端发送请求，获取响应的结果
        IndexResponse response = restHighLevelClient.index(request, RequestOptions.DEFAULT);
        System.out.println(response.status());// 获取建立索引的状态信息 CREATED
        System.out.println(response);// 查看返回内容
    }
```

# Spring Data ElasticSearch

## 简介

- [参考教程](https://www.macrozheng.com/mall/architect/mall_arch_07.html#spring-data-elasticsearch)
- 基于 spring data API 简化ES操作，将原始操作ES的客户端API进行封装

## 常用注解

- @Document：作用在类上，标记实体类为文档对象
- @Id：作用在成员变量上，标记文档的id
- @Feild：作用在成员变量，标记为文档的字段，并制定映射属性

```java
/**
 * 搜索商品的信息
 * Created by macro on 2018/6/19.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Document(indexName = "pms", type = "product",shards = 1,replicas = 0)
public class EsProduct implements Serializable {
    private static final long serialVersionUID = -1L;
    @Id
    private Long id;
    @Field(type = FieldType.Keyword)
    private String productSn;
    private Long brandId;
    @Field(type = FieldType.Keyword)
    private String brandName;
    private Long productCategoryId;
    @Field(type = FieldType.Keyword)
    private String productCategoryName;
    private String pic;
    @Field(analyzer = "ik_max_word",type = FieldType.Text)
    private String name;
    @Field(analyzer = "ik_max_word",type = FieldType.Text)
    private String subTitle;
    @Field(analyzer = "ik_max_word",type = FieldType.Text)
    private String keywords;
    @Field(type =FieldType.Nested)
    private List<EsProductAttributeValue> attrValueList;
}
```

## 衍生查询

- 在接口中直接指定查询方法名称便可查询，无需进行实现，如商品表中有商品名称、标题和关键字，直接定义以下查询，就可以对这三个字段进行全文搜索
- 原理很简单，就是将一定规则方法名称的方法转化为Elasticsearch的Query DSL语句
- 在idea中直接会提示对应字段

```java
/**
 * 搜索查询
 *
 * @param name              商品名称
 * @param subTitle          商品标题
 * @param keywords          商品关键字
 * @param page              分页信息
 * @return
 */
Page<EsProduct> findByNameOrSubTitleOrKeywords(String name, String subTitle, String keywords, Pageable page);
```





# 全文检索和数据库搜索的区别

全文检索分为两大部分：索引流程、搜索流程。
索引流程：即采集数据构建文档对象分析文档（分词）创建索引。
搜索流程：即用户通过搜索界面创建查询执行搜索，搜索器从索引库搜索渲染搜索结果
1、索引流程
从原始文件中提取一些可以用来搜索的数据（封装成各种Field），把各field再封装成document，然后对document进行分析（对各字段分词），得到一些索引目录写入索引库，document本身也会被写入一个文档信息库；
2、搜索流程
根据关键词解析出（queryParser）查询条件query(TermQuery)
然后利用搜索工具（indexSearcher）去索引库获取文档id
然后再根据文档id去文档信息库获取文档信息。
最后会返回一系列文档
分词器不同，建立的索引数据就不同

# EasyES框架

[文档](https://www.easy-es.cn/pages/2688d1/)
