[toc]

# 平均负载（load average）

## 概念
- 平均负载：是指系统处于`可运行状态`和`不可中断状态`的平均进程数，也就是平均活跃进程数，和CPU使用率没有直接关系
    - 可运行状态的进程，是指正在使用CPU或者正在等待CPU的进程，也就是我们常用`ps`命令看到的，处于`R`状态（`Running 或 Runnable`）的进程
    - 不可中断状态的进程则是正处于内核态关键流程中的进程，并且这些流程是不可打断的，比如最常见的是等待硬件设备的I/O响应，也就是我们在`ps`命令中看到的`D`状态（`Uninterruptible Sleep，也称为Disk Sleep`）的进程
- 可以简单理解为，平均负载其实就是平均活跃进程数。平均活跃进程数，直观上的理解就是单位时间（`这个时间是指1分钟？？？`）内的活跃进程数，但它实际上是活跃进程数的`指数衰减平均值`。这个“指数衰减平均”的详细含义你不用计较，这只是系统的一种更快速的计算方式，你把它直接当成活跃进程数的平均值也没问题

## 平均负载 VS CPU使用率
- 既然平均负载代表的是活跃进程数，那平均负载高了，不就意味着 CPU 使用率高吗？
- No, 平均负载是指单位时间内，处于可运行状态和不可中断状态的进程数。所以，它不仅包括了正在使用 CPU 的进程，还包括等待 CPU 和等待 I/O 的进程
- 而 CPU 使用率，是单位时间内 CPU 繁忙情况的统计，跟平均负载并不一定完全对应。比如：
    - CPU 密集型进程，使用大量 CPU 会导致平均负载升高，此时这两者是一致的；
    - I/O 密集型进程，等待 I/O 也会导致平均负载升高，但 CPU 使用率不一定很高；
    - 大量等待 CPU 的进程调度也会导致平均负载升高，此时的CPU使用率也会比较高。
- 为什么平均负载还包括等待 CPU 和等待 I/O 的进程，一个比喻：一台计算机比喻成一辆地铁，地铁的乘客容量就是CPU个数，正在使用CPU的进程就是在地铁上的人；等待CPU的进程就是在下一站等地铁来的人；等待I/O的进程就是在下一站要上车和下车的人，虽然现在对CPU没影响，可未来会影响，所以也要考虑到平均负载上。

## 平均负载的理想值
- 平均负载的理想值是等于 CPU 个数，这样每个CPU都得到了充分利用。当平均负载比 CPU 个数还大的时候，说明系统出现了过载。比如当平均负载为2时，意味着什么呢？
    - 在只有2个CPU的系统上，意味着所有的CPU都刚好被完全占用。
    - 在4个CPU的系统上，意味着CPU有50%的空闲。
    - 而在只有1个CPU的系统中，则意味着有一半的进程竞争不到CPU。

## Linux如何查看cpu个数？
- 可以通过 top 命令或者从文件 /proc/cpuinfo 中读取
```shell
# 关于grep和wc的用法请查询它们的手册或者网络搜索
$ grep 'model name' /proc/cpuinfo | wc -l
```

## 使用uptime命令查看平均负载
- 查看当前时间、系统运行时间、正在登录用户数、==过去1/5/15分钟的平均负载==
- 可以看到，平均负载有三个数值，到底该参考哪一个呢？
- 实际上，都要看。三个不同时间间隔的平均值，其实给我们提供了，分析系统负载==趋势==的数据来源，让我们能更全面、更立体地理解目前的负载状况
    - 如果1分钟、5分钟、15分钟的三个值基本相同，或者相差不大，那就说明系统负载很平稳。
    - 但如果1分钟的值远小于15 分钟的值，就说明系统最近1分钟的负载在减少，而过去15分钟内却有很大的负载。
    - 反过来，如果1分钟的值远大于 15 分钟的值，就说明最近1分钟的负载在增加，这种增加有可能只是临时性的，也有可能还会持续增加下去，所以就需要持续观察。一旦1分钟的平均负载接近或超过了CPU的个数，就意味着系统正在发生过载的问题，这时就得分析调查是哪里导致的问题，并要想办法优化了。
- 举个例子，假设我们在一个单 CPU 系统上看到平均负载为 1.73，0.60，7.98，那么说明在过去 1 分钟内，系统有 73% 的超载，而在 15 分钟内，有 698% 的超载，从整体趋势来看，系统的负载在降低

### 那么，在实际生产环境中，平均负载多高时，需要我们重点关注呢？
- 在我看来，当`平均负载高于CPU数量70%的时候`，你就应该分析排查负载高的问题了。一旦负载过高，就可能导致进程响应变慢，进而影响服务的正常功能。
- 但70%这个数字并不是绝对的，最推荐的方法，还是==把系统的平均负载监控起来，然后根据更多的历史数据，判断负载的变化趋势。当发现负载有明显升高趋势时，比如说负载翻倍了，你再去做分析和调查==。

## 平均负载案例分析
- 简单介绍一下 stress 和 sysstat
- stress 是一个 Linux 系统压力测试工具，这里我们用作异常进程模拟平均负载升高的场景
- sysstat 包含了常用的 Linux 性能工具，用来监控和分析系统的性能, 我们的案例会用到这个包的两个命令 mpstat 和 pidstat。
    - mpstat 是一个常用的多核 CPU 性能分析工具，用来实时查看每个 CPU 的性能指标，以及所有CPU的平均指标。
    - pidstat 是一个常用的进程性能分析工具，用来实时查看进程的 CPU、内存、I/O 以及上下文切换等性能指标。


### 场景一：模拟 CPU 密集型进程
- 首先，在第一个终端运行 stress 命令，模拟一个 CPU 使用率 100% 的场景：
```shell
$ stress --cpu 1 --timeout 600
```
- 第二个终端运行uptime查看平均负载的变化情况：
```shell
$ watch -d uptime
...,  load average: 1.00, 0.75, 0.39
```
- watch是Linux自带的一个工具，用来周期性的执行一个程序，并全屏查看输出。`-d` 参数表示高亮显示变化的区域

- 第三个终端运行mpstat查看 CPU 使用率的变化情况：
```shell
# -P ALL 表示监控所有CPU，后面数字5表示间隔5秒后输出一组数据
$ mpstat -P ALL 5
Linux 4.15.0 (ubuntu) 09/22/18     _x86_64_     (2 CPU)
13:30:06     CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest  %gnice   %idle
13:30:11     all   50.05    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00   49.95
13:30:11       0    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00  100.00
13:30:11       1  100.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00
```
- pidstat查看进程状态：
```shell
# 间隔5秒后输出一组数据
$ pidstat -u 5 1
13:37:07      UID       PID    %usr %system  %guest   %wait    %CPU   CPU  Command
13:37:12        0      2962  100.00    0.00    0.00    0.00  100.00     1  stress
```

### 场景二：模拟 I/O 密集型进程
- 运行 stress 命令，但这次模拟 I/O 压力，即不停地执行 sync：
```shell
$ stress -i 1 --timeout 600
```
### 场景三：模拟大量进程的场景
- 当系统中运行进程超出 CPU 运行能力时，就会出现等待 CPU 的进程。
- 使用 stress 模拟 8 个进程：
```shell
$ stress -c 8 --timeout 600
```
- 由于系统只有 2 个CPU，明显比 8 个进程要少得多，因而，系统的 CPU 处于严重过载状态，平均负载高达7.97

