## awk命令

> [参考](https://blog.csdn.net/zhangcongyi420/article/details/125692179)

### 将数字按三分位逗号隔开（小数点后2位）

`echo '123456789.12'| awk '{printf "% '"'"' 8.2f\n",$0}'`

### 按制表符分隔参数

如果参数中有多个空格，awk默认通过空格分隔参数，如果不希望这样，可以指定分隔符

`"${sql4}" | awk -F '\t' '{print $1","$2}'`
