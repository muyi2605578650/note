

# day02 初步制作IPL、使用Makefile

- IPL工作流程：`开机后BIOS会自动将IPL加载到ORG指令指定的内存地址0x7c00-0x7dff，然后IPL会开始调用读盘的中断处理程序，将软盘的后续扇区加载到内存，这些扇区中包含了OS程序`
  - **BIOS** 是写入到主板上的固件 (写入到硬件的程序)，启动时，它会加载 CMOS 信息，检测硬件配置，进行自检，初始化硬件，然后寻找能启动的硬盘并读取启动硬盘中的系统引导程序 (Boot loader)。
  - https://blog.csdn.net/ymz641/article/details/121404818
- makefile处理过程：
  - 输入命令-->make.exe启动-->它首先读取makefile文件-->之后就自动按照makefile里的逻辑执行
  - make.exe可以判断输入文件是否存在、文件的更新日期等



farjmp跳转后怎么跳回来的？



# 本书中UI接口功能整理（方便快速查阅）

boxfill8 往矩形中填充颜色

putfont8 描绘单个字符

putfonts8_asc 描绘字符串

putfonts8_asc_sht 在指定图层的指定区域内描绘字符串，并刷新图层

putblock8_8 在指定的矩形区域绘制buf中的图像，如绘制鼠标

make_textbox8 描绘文字输入的背景

make_window8 描绘一个窗口

# 色号

\#000000：黑 #00ffff：浅亮蓝 #000084：暗蓝 #ff0000：亮红 #ffffff：白 #840084：暗紫 #00ff00：亮绿 #c6c6c6：亮灰 #008484：浅暗蓝 #ffff00：亮黄 #840000：暗红 #848484：暗灰 #0000ff：亮蓝 #008400：暗绿 #ff00ff：亮紫 #848400：暗黄
